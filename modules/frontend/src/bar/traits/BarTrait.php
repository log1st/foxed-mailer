<?php
	/**
	 * Created by PhpStorm.
	 * User: log1st
	 * Date: 22.01.2018
	 * Time: 3:31
	 */
	
	namespace frontend\src\bar\traits;
	
	
	use frontend\src\action\factories\ActionFactory;
	
	trait BarTrait
	{
		/**
		 * @var ActionFactory
		 */
		private $items = [];
		
		protected function addItem(ActionFactory $data)
		{
			$this->items[] = $data;
		}
		
		public function getItems() {
			return $this->items;
		}
	}