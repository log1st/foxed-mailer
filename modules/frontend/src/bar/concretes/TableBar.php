<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use frontend\src\bar\abstractions\AbstractBar;
	use frontend\src\bar\abstractions\AbstractTableBar;
	use frontend\src\bar\concretes\table\AccountsBar;
	use frontend\src\bar\concretes\table\CompaniesBar;
	use frontend\src\bar\concretes\table\DomainsBar;
	use frontend\src\bar\concretes\table\LanguagesBar;
	use frontend\src\bar\concretes\table\MessagesBar;
	use frontend\src\bar\concretes\table\SourceMessagesBar;
	use frontend\src\bar\concretes\table\UsersBar;
	use yii\web\HttpException;
	
	class TableBar extends AbstractBar
	{
		protected $keys = [
			'companies' => CompaniesBar::class,
			'accounts' => AccountsBar::class,
			'domains' => DomainsBar::class,
			'source-messages' => SourceMessagesBar::class,
			'messages' => MessagesBar::class,
			'languages' => LanguagesBar::class,
			'users' => UsersBar::class
		];
		
		/** @var AbstractTableBar */
		protected $table;
		
		protected function init($route, $params)
		{
			if(!in_array($params['key'] ?? null, array_keys($this->keys))) {
				throw new HttpException(400, t('controller', 'rest.get-bar.error.unknownRoute'));
			}
			$this->setTitle('table.' . $params['key']);
			
			$this->setMeta('controls', [
				'table-filters',
				'table-controls'
			]);
			
			$table = $this->keys[$params['key']];
			/** @var AbstractTableBar $table */
			$table = new $table($route, $params);
			
			$this->table = $table;
			
			foreach($table->getItems() as $field) {
				$this->addItem($field);
			}
		}
		
		protected function metaClass(): string
		{
			return $this->table->metaClass();
		}
	}