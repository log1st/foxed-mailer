<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use action\sourceMessage\Edit;
	use action\sourceMessage\EditCategory;
	use action\sourceMessage\EditMessage;
	use frontend\models\SourceMessage;
	use frontend\src\bar\abstractions\AbstractBar;
	
	class SourceMessageViewBar extends AbstractBar
	{
		protected function init($route, $params)
		{
			$this->setTitle('source-message', [
				'id' => \Yii::$app->request->get('params')['id'] ?? ''
			]);
			
			$source = SourceMessage::find()->where([
				SourceMessage::idStr() => $params['id'] ?? null
			])->one();
			
			$this->addItem(new Edit($source));
			$this->addItem(new EditCategory($source));
			$this->addItem(new EditMessage($source));
		}
		
		protected function metaClass(): string
		{
			return SourceMessage::shortClassName();
		}
	}