<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use frontend\src\bar\abstractions\AbstractBar;
	
	class SignInBar extends AbstractBar
	{
		
		protected function init($route, $params)
		{
			$this->setTitle('sign-in');
		}
		
		protected function metaClass(): string
		{
			return '_SignIn';
		}
	}