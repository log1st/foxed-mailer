<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use action\company\Edit;
	use action\company\EditName;
	use action\company\EditToken;
	use frontend\models\Company;
	use frontend\src\bar\abstractions\AbstractBar;
	
	class CompanyViewBar extends AbstractBar
	{
		protected function init($route, $params)
		{
			$this->setTitle('company', [
				'id' => \Yii::$app->request->get('params')['id'] ?? ''
			]);
			
			$company = Company::find()->where([
				Company::idStr() => $params[ 'id' ] ?? null
			])->one();
			
			$this->addItem(new Edit($company));
			$this->addItem(new EditName($company));
			$this->addItem(new EditToken($company));
			
		}
		
		protected function metaClass(): string
		{
			return Company::shortClassName();
		}
	}