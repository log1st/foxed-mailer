<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use frontend\models\Account;
	use frontend\src\bar\abstractions\AbstractBar;
	
	class AccountViewBar extends AbstractBar
	{
		protected function init($route, $params)
		{
			$this->setTitle('account', [
				'id' => \Yii::$app->request->get('params')[ 'id' ] ?? ''
			]);
		}
		
		protected function metaClass(): string
		{
			return Account::shortClassName();
		}
	}