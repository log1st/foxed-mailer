<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.01.2018
	 * Time: 5:49
	 */
	
	namespace frontend\src\bar\concretes\table;
	
	
	use action\sourceMessage\Create;
	use frontend\models\SourceMessage;
	use frontend\src\bar\abstractions\AbstractTableBar;
	
	class SourceMessagesBar extends AbstractTableBar
	{
		public function __construct($route, $params)
		{
			$this->addItem(new Create(new SourceMessage()));
		}
		
		public function metaClass(): string
		{
			return SourceMessage::shortClassName();
		}
	}