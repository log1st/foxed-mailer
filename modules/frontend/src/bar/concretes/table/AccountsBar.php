<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.01.2018
	 * Time: 5:49
	 */
	
	namespace frontend\src\bar\concretes\table;
	
	
	use frontend\models\Account;
	use frontend\src\bar\abstractions\AbstractTableBar;
	
	class AccountsBar extends AbstractTableBar
	{
		public function __construct($route, $params)
		{
			// TODO: Implement getFields() method.
		}
		
		public function metaClass(): string
		{
			return Account::shortClassName();
		}
	}