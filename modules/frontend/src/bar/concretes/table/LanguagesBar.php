<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.01.2018
	 * Time: 5:49
	 */
	
	namespace frontend\src\bar\concretes\table;
	
	
	use action\language\Create;
	use frontend\models\Language;
	use frontend\src\bar\abstractions\AbstractTableBar;
	
	class LanguagesBar extends AbstractTableBar
	{
		public function __construct($route, $params)
		{
			$this->addItem(new Create(new Language()));
		}
		
		public function metaClass(): string
		{
			return Language::shortClassName();
		}
	}