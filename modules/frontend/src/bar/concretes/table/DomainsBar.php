<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.01.2018
	 * Time: 5:49
	 */
	
	namespace frontend\src\bar\concretes\table;
	
	
	use action\domain\Create;
	use frontend\models\Domain;
	use frontend\src\bar\abstractions\AbstractTableBar;
	
	class DomainsBar extends AbstractTableBar
	{
		public function __construct($route, $params)
		{
			$this->addItem(new Create(new Domain()));
		}
		
		public function metaClass(): string
		{
			return Domain::shortClassName();
		}
	}