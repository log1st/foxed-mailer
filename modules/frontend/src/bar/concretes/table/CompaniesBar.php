<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.01.2018
	 * Time: 5:49
	 */
	
	namespace frontend\src\bar\concretes\table;
	
	
	
	use action\company\Create;
	use frontend\models\Company;
	use frontend\src\bar\abstractions\AbstractTableBar;
	
	class CompaniesBar extends AbstractTableBar
	{
		public function __construct($route, $params)
		{
			$this->addItem(new Create(new Company()));
		}
		
		public function metaClass(): string
		{
			return Company::shortClassName();
		}
	}