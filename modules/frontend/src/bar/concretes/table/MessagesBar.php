<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.01.2018
	 * Time: 5:49
	 */
	
	namespace frontend\src\bar\concretes\table;
	
	
	use action\message\Create;
	use frontend\models\Message;
	use frontend\src\bar\abstractions\AbstractTableBar;
	use frontend\src\bar\concretes\TableBar;
	
	class MessagesBar extends AbstractTableBar
	{
		public function __construct($route, $params)
		{
			$this->addItem(new Create(new Message()));
		}
		
		public function metaClass(): string
		{
			return Message::shortClassName();
		}
	}