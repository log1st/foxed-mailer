<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.01.2018
	 * Time: 5:49
	 */
	
	namespace frontend\src\bar\concretes\table;
	
	
	use action\user\Create;
	use frontend\models\User;
	use frontend\src\bar\abstractions\AbstractTableBar;
	
	class UsersBar extends AbstractTableBar
	{
		public function __construct($route, $params)
		{
			$this->addItem(new Create(new User()));
		}
		
		public function metaClass(): string
		{
			return User::shortClassName();
		}
	}