<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use action\message\Edit;
	use action\message\EditLanguage;
	use action\message\EditSource;
	use action\message\EditTranslation;
	use frontend\models\Language;
	use frontend\models\Message;
	use frontend\src\bar\abstractions\AbstractBar;
	
	class MessageViewBar extends AbstractBar
	{
		protected function init($route, $params)
		{
			$this->setTitle('message', [
				'id' => $params['id'] ?? null
			]);
			
			$message = Message::find()->where([
				Message::idStr() => $params['id'] ?? null
			])->one();
			
			$this->addItem(new Edit($message));
			$this->addItem(new EditSource($message));
			$this->addItem(new EditLanguage($message));
			$this->addItem(new EditTranslation($message));
			
		}
		
		protected function metaClass(): string
		{
			return Language::shortClassName();
		}
	}