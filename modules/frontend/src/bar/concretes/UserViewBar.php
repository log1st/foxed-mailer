<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use action\user\Edit;
	use action\user\EditEmail;
	use action\user\EditPassword;
	use action\user\ManagePermissions;
	use frontend\models\User;
	use frontend\src\bar\abstractions\AbstractBar;
	
	class UserViewBar extends AbstractBar
	{
		protected function init($route, $params)
		{
			$this->setTitle('user', [
				'id' => \Yii::$app->request->get('params')['id'] ?? ''
			]);
			
			$user = User::find()->where([
				User::idStr() => $params['id'] ?? null
			])->one();
			
			$this->addItem(new Edit($user));
			$this->addItem(new EditEmail($user));
			$this->addItem(new EditPassword($user));
			$this->addItem(new ManagePermissions($user));
		}
		
		protected function metaClass(): string
		{
			return User::shortClassName();
		}
	}