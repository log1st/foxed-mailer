<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use action\domain\Edit;
	use action\domain\EditCompany;
	use action\domain\EditHost;
	use frontend\models\Domain;
	use frontend\src\bar\abstractions\AbstractBar;
	
	class DomainViewBar extends AbstractBar
	{
		protected function init($route, $params)
		{
			$this->setTitle('domain', [
				'id' => \Yii::$app->request->get('params')['id'] ?? ''
			]);
			
			$domain = Domain::find()->where([
				Domain::idStr() => $params[ 'id' ] ?? null
			])->one();
			
			$this->addItem(new Edit($domain));
			$this->addItem(new EditHost($domain));
			$this->addItem(new EditCompany($domain));
		}
		
		protected function metaClass(): string
		{
			return Domain::shortClassName();
		}
	}