<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 21:16
	 */
	
	namespace frontend\src\bar\concretes;
	
	
	use action\language\Edit;
	use action\language\EditCountry;
	use action\language\EditLocale;
	use action\language\EditName;
	use action\language\EditStatus;
	use frontend\models\Language;
	use frontend\src\bar\abstractions\AbstractBar;
	
	class LanguageViewBar extends AbstractBar
	{
		protected function init($route, $params)
		{
			$this->setTitle('language', [
				'id' => \Yii::$app->request->get('params')[ 'id' ] ?? ''
			]);
			
			$message = Language::find()->where([
				Language::idStr() => $params[ 'id' ] ?? null
			])->one();
			
			$this->addItem(new Edit($message));
			$this->addItem(new EditCountry($message));
			$this->addItem(new EditLocale($message));
			$this->addItem(new EditName($message));
			$this->addItem(new EditStatus($message));
		}
		
		protected function metaClass(): string
		{
			return Language::shortClassName();
		}
	}