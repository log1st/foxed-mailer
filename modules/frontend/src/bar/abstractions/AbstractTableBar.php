<?php
	/**
	 * Created by PhpStorm.
	 * User: log1st
	 * Date: 22.01.2018
	 * Time: 3:27
	 */
	
	namespace frontend\src\bar\abstractions;
	
	
	use frontend\src\bar\traits\BarTrait;
	
	abstract class AbstractTableBar
	{
		use BarTrait;
		
		abstract public function __construct($route, $params);
		
		abstract public function metaClass() : string;
	}