<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:13
	 */
	
	namespace frontend\src\bar\abstractions;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\bar\traits\BarTrait;
	use yii\base\InvalidConfigException;
	
	abstract class AbstractBar
	{
		use BarTrait;
		
		abstract protected function init($route, $params);
		
		abstract protected function metaClass() : string;
		
		public function setTitle($value, $params = [])
		{
			$this->setMeta('title', $value);
			if (count($params)) {
				$this->setMeta('title-params', $params);
			}
		}
		
		/** @var array */
		private $meta = [];
		
		public function __construct($route, $params = [])
		{
			$this->init($route, $params);
		}
		
		protected function setMeta($key, $value)
		{
			$this->meta[$key] = $value;
		}
		
		public function getData()
		{
			return [
				'meta' => $this->meta,
				'items' => $this->getItems(),
			];
		}
	}