<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\models\Message;
	use frontend\src\transform\AbstractTransformer;
	
	class MessageTransformer extends AbstractTransformer
	{
		public static $loads = [
			'_source',
			'_language'
		];
		/**
		 * @param BaseActiveRecord|Message $message
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $message): \stdClass
		{
			return (object)ArrayHelper::merge(
				array_filter($message->attributes, function ($key) {
					return in_array($key, [
						Message::idStr(),
						'source_id',
						'message_id',
						'translation'
					]);
				}, ARRAY_FILTER_USE_KEY),
				[
					'_source' => (new SourceMessageTransformer)->one($message->_source),
					'_language' => (new LanguageTransformer)->one($message->_language),
				]
			);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'messages.view']
				]
			];
		}
	}