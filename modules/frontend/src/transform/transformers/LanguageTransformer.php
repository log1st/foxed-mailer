<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\models\Language;
	use frontend\src\transform\AbstractTransformer;
	
	class LanguageTransformer extends AbstractTransformer
	{
		public static $loads = [
			'_country'
		];
		/**
		 * @param BaseActiveRecord|Language $language
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $language): \stdClass
		{
			return (object)ArrayHelper::merge(
				array_filter($language->attributes, function ($key) {
					return in_array($key, [
						Language::idStr(),
						'locale',
						'name',
						'status',
						'country',
						'created_at',
					]);
				}, ARRAY_FILTER_USE_KEY),
				[
					'_country' => (new CountryTransformer)->one($language->_country)
				]
			);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'languages.view']
				]
			];
		}
	}