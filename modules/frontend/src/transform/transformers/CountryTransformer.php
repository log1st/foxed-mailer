<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\models\Country;
	use frontend\src\transform\AbstractTransformer;
	
	class CountryTransformer extends AbstractTransformer
	{
		/**
		 * @param BaseActiveRecord|Country $country
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $country): \stdClass
		{
			return (object)
			array_filter($country->attributes, function ($key) {
				return in_array($key, [
					Country::idStr(),
					'code',
					'name',
				]);
			}, ARRAY_FILTER_USE_KEY);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true
				]
			];
		}
	}