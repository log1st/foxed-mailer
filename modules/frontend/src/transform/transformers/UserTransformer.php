<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use common\models\auth\Assignment;
	use common\models\auth\Item;
	use frontend\models\User;
	use frontend\src\transform\AbstractTransformer;
	
	class UserTransformer extends AbstractTransformer
	{
		public static $loads = [
			'_assignments._item'
		];
		/**
		 * @param BaseActiveRecord|User $user
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $user): \stdClass
		{
			$user = (object)ArrayHelper::merge(
				array_filter($user->attributes, function ($key) {
					return in_array($key, [
						User::idStr(),
						'email',
						'created_at',
					]);
				}, ARRAY_FILTER_USE_KEY),
				[
					'roles' => array_values(array_map(function ($item) {
						/** @var Assignment $item */
						return $item->_item->name;
					}, array_filter($user->_assignments, function ($item) {
						return $item->_item->type === Item::TYPE_ROLE;
					}))),
					'permissions' => array_values(array_map(function ($item) {
						/** @var Assignment $item */
						return $item->_item->name;
					}, array_filter($user->_assignments, function ($item) {
						return $item->_item->type === Item::TYPE_PERMISSION;
					}))),
				]
			);
			
			sort($user->permissions);
			sort($user->roles);
			
			return $user;
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'users.view']
				]
			];
		}
	}