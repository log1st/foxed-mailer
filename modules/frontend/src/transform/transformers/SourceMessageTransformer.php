<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\models\SourceMessage;
	use frontend\src\transform\AbstractTransformer;
	
	class SourceMessageTransformer extends AbstractTransformer
	{
		/**
		 * @param BaseActiveRecord|SourceMessage $language
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $language): \stdClass
		{
			return (object)ArrayHelper::merge(
				array_filter($language->attributes, function ($key) {
					return in_array($key, [
						SourceMessage::idStr(),
						'category',
						'message',
						'created_at'
					]);
				}, ARRAY_FILTER_USE_KEY),
				[
				
				]
			);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'source-messages.view']
				]
			];
		}
	}