<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\models\Domain;
	use frontend\src\transform\AbstractTransformer;
	
	class DomainTransformer extends AbstractTransformer
	{
		public static $loads = [
			'_company'
		];
		/**
		 * @param BaseActiveRecord|Domain $domain
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $domain): \stdClass
		{
			return (object)
			ArrayHelper::merge(array_filter($domain->attributes, function ($key) {
				return in_array($key, [
					Domain::idStr(),
					'host',
					'company',
					'created_at',
					'updated_at'
				]);
			}, ARRAY_FILTER_USE_KEY), [
				'_company' => (new CompanyTransformer())->one($domain->_company)
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'client', 'domains.view']
				]
			];
		}
	}