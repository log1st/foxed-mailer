<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\models\Company;
	use frontend\src\transform\AbstractTransformer;
	
	class CompanyTransformer extends AbstractTransformer
	{
		public static $loads = [
			'_user._assignments._item'
		];
		/**
		 * @param BaseActiveRecord|Company $company
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $company): \stdClass
		{
			return (object)
			ArrayHelper::merge(
				array_filter($company->attributes, function ($key) {
					return in_array($key, [
						Company::idStr(),
						'name',
						'user',
						'token',
						'created_at',
						'updated_at'
					]);
				}, ARRAY_FILTER_USE_KEY),
				[
					'_user' => (new UserTransformer())->one($company->_user)
				]
			);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'client', 'companies.view']
				]
			];
		}
	}