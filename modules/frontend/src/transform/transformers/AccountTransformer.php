<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:55
	 */
	
	namespace frontend\src\transform\transformers;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\models\Account;
	use frontend\src\transform\AbstractTransformer;
	
	class AccountTransformer extends AbstractTransformer
	{
		public static $loads = [
			'_company'
		];
		/**
		 * @param BaseActiveRecord|Account $account
		 * @return \stdClass
		 */
		protected function transform(BaseActiveRecord $account): \stdClass
		{
			return (object)
			ArrayHelper::merge(array_filter($account->attributes, function ($key) {
				return in_array($key, [
					Account::idStr(),
					'name',
					'company',
					'encryption',
					'host',
					'port',
					'login',
					'password',
					'created_at',
					'updated_at'
				]);
			}, ARRAY_FILTER_USE_KEY), [
				'_company' => (new CompanyTransformer())->one($account->_company)
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'client', 'accounts.view']
				]
			];
		}
	}