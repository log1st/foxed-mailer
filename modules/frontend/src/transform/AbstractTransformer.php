<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 17.01.2018
	 * Time: 4:48
	 */
	
	namespace frontend\src\transform;
	
	
	use common\components\BaseActiveRecord;
	
	abstract class AbstractTransformer
	{
		public static $loads = [];
		
		abstract protected function transform(BaseActiveRecord $item) : \stdClass;
		
		abstract public function accessRules();
		
		public function one(BaseActiveRecord $record) : \stdClass {
			return $this->transform($record);
		}
		
		public function all(array $records) : array {
			return array_map(function($record) {
				return $this->one($record);
			}, $records);
		}
	}