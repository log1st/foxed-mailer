<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 10.01.2018
	 * Time: 23:43
	 */
	
	namespace frontend\src\table\requests;
	
	
	use yii\base\Model;
	
	class TableRequest extends Model
	{
		const DEFAULT_LIMIT = 10;
		const LIMITS = [
			5, 10, 15, 20, 25, 50
		];
		
		public $limit;
		public $orders;
		public $filters;
		public $page;
		
		public function scenarios()
		{
			return [
				'index' => ['limit', 'orders', 'filters', 'page']
			];
		}
		
		public function rules() {
			return [
				[['limit'], 'limit', 'skipOnEmpty' => false],
				[['orders'], 'orders', 'skipOnEmpty' => false],
				[['filters'], 'filters', 'skipOnEmpty' => false],
				[['page'], 'page', 'skipOnEmpty' => false]
			];
		}
		
		public function limit($attribute, $params) {
			if(!in_array($this->{$attribute}, self::LIMITS)) {
				$this->{$attribute} = self::DEFAULT_LIMIT;
			}
		}
		
		public function orders($attribute, $params) {
		
		}
		
		public function filters($attribute, $params) {
		
		}
		
		public function page($attribute, $params) {
			if(!isset($this->{$attribute})) {
				$this->{$attribute} = 1;
			}
			
			$this->{$attribute} = $this->{$attribute} - 1;
		}
	}