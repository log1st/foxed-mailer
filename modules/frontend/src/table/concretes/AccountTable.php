<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:12
	 */
	
	namespace frontend\src\table\concretes;
	
	
	use common\components\BaseActiveRecord;
	use frontend\models\Account;
	use frontend\models\Company;
	use frontend\src\table\abstractions\AbstractTable;
	
	class AccountTable extends AbstractTable
	{
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getModel(): BaseActiveRecord
		{
			return new Account([
				'scenario' => 'index',
			]);
		}
		
		protected function init()
		{
			$this->setRelations([
				'_company',
			]);
			
			$this->addColumn((object)[
				'key' => Account::idStr(),
				'type' => 'link',
				'params' => [
					'key' => Account::idStr(),
					'route' => 'account',
					'value' => [ Account::idStr() ],
					'access' => [
						['roles' => ['admin', 'client']],
						['roles' => ['manager'], 'permissions' => ['accounts.view']]
					]
				]
			]);
			
			$this->addColumn((object)[
				'key' => 'name',
			]);
			
			$this->addColumn((object)[
				'key' => 'company',
				'type' => 'relation',
				'params' => (object)[
					'relation' => '_company',
					'key' => Company::idStr(),
					'select' => [
						Company::idStr(),
						'name',
					],
					'value' => ['name'],
					'table' => Company::tableName(),
					'access' => [
						['roles' => ['admin', 'client']],
						['roles' => ['manager'], 'permissions' => ['companies.view']]
					],
					'route' => 'company',
				],
			]);
			
			$this->addColumn((object)[
				'key' => 'host',
			]);
			
			$this->addColumn((object)[
				'key' => 'port',
			]);
			
			$this->addColumn((object)[
				'key' => 'encryption',
			]);
			
			$this->addColumn((object)[
				'key' => 'created_at',
				'type' => 'date',
			]);
			
			$this->addColumn((object)[
				'key' => 'updated_at',
				'type' => 'date',
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'client', 'accounts.index']
				]
			];
		}
	}