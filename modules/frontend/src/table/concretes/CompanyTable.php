<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:12
	 */
	
	namespace frontend\src\table\concretes;
	
	
	use common\components\BaseActiveRecord;
	use frontend\models\Company;
	use frontend\src\table\abstractions\AbstractTable;
	
	class CompanyTable extends AbstractTable
	{
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getModel(): BaseActiveRecord
		{
			return new Company([
				'scenario' => 'index'
			]);
		}
		
		protected function init()
		{
			$this->addColumn((object)[
				'key' => Company::idStr(),
				'type' => 'link',
				'params' => [
					'key' => Company::idStr(),
					'route' => 'company',
					'value' => [ Company::idStr() ],
					'access' => [
						['roles' => ['admin', 'client']],
						['roles' => ['manager'], 'permissions' => ['companies.view']]
					]
				]
			]);
			
			$this->addColumn((object)[
				'key' => 'name'
			]);
			
			$this->addColumn((object)[
				'key' => 'token',
				'params' => [
					'max' => 10
				]
			]);
			
			$this->addColumn((object)[
				'key' => 'created_at',
				'type' => 'date'
			]);
			
			$this->addColumn((object)[
				'key' => 'updated_at',
				'type' => 'date'
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'client', 'companies.index']
				]
			];
		}
	}