<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:12
	 */
	
	namespace frontend\src\table\concretes;
	
	
	use common\components\BaseActiveRecord;
	use frontend\models\Language;
	use frontend\models\Message;
	use frontend\src\table\abstractions\AbstractTable;
	
	class MessageTable extends AbstractTable
	{
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getModel(): BaseActiveRecord
		{
			return new Message([
				'scenario' => 'index',
			]);
		}
		
		protected function init()
		{
			$this->setRelations([
				'_source',
				'_language'
			]);
			
			$this->addColumn((object)[
				'key' => Message::idStr(),
				'type' => 'link',
				'params' => [
					'key' => Message::idStr(),
					'route' => 'message',
					'value' => [ Message::idStr() ],
					'access' => [
						['roles' => ['admin']],
						['permissions' => ['messages.view']]
					]
				]
			]);
			
			/*
			$this->addColumn((object)[
				'key' => 'source',
				'type' => 'relation',
				'params' => (object)[
					'cut' => 40,
					'relation' => '_source',
					'key' => SourceMessage::idStr(),
					'select' => [
						SourceMessage::idStr(),
						'category',
						'message'
					],
					'value' => ['message'],
					'table' => SourceMessage::tableName(),
					'access' => [
						['roles' => ['admin']],
						['permissions' => ['source-messages.view']]
					],
					'route' => 'source-message',
				],
			]);
			*/
			
			$this->addColumn((object)[
				'key' => 'language',
				'type' => 'relation',
				'params' => (object)[
					'relation' => '_language',
					'key' => Language::idStr(),
					'select' => [
						Language::idStr(),
						'name'
					],
					'value' => ['name'],
					'table' => Language::tableName(),
					'access' => [
						['roles' => ['admin']],
						['permissions' => ['languages.view']]
					],
					'route' => 'language',
				],
			]);
			
			$this->addColumn((object)[
				'key' => 'translation'
			]);
			
			$this->addColumn((object)[
				'key' => 'created_at',
				'type' => 'date',
			]);
			
			$this->addColumn((object)[
				'key' => 'updated_at',
				'type' => 'date',
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'messages.index']
				]
			];
		}
	}