<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:12
	 */
	
	namespace frontend\src\table\concretes;
	
	
	use common\components\BaseActiveRecord;
	use frontend\models\User;
	use frontend\src\table\abstractions\AbstractTable;
	
	class UserTable extends AbstractTable
	{
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getModel(): BaseActiveRecord
		{
			return new User([
				'scenario' => 'index'
			]);
		}
		
		protected function init()
		{
			$this->addColumn((object)[
				'key' => User::idStr(),
				'type' => 'link',
				'params' => [
					'key' => User::idStr(),
					'route' => 'user',
					'value' => [ User::idStr() ],
					'access' => [
						['roles' => ['admin']],
						['permissions' => ['users.view']]
					]
				]
			]);
			
			$this->addColumn((object)[
				'key' => 'email'
			]);
			
			$this->addColumn((object)[
				'key' => 'created_at',
				'type' => 'date'
			]);
			
			$this->addColumn((object)[
				'key' => 'updated_at',
				'type' => 'date'
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'users.index']
				]
			];
		}
	}