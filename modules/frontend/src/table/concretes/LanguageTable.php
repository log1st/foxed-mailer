<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:12
	 */
	
	namespace frontend\src\table\concretes;
	
	
	use common\components\BaseActiveRecord;
	use common\models\Country;
	use frontend\models\Language;
	use frontend\src\table\abstractions\AbstractTable;
	
	class LanguageTable extends AbstractTable
	{
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getModel(): BaseActiveRecord
		{
			return new Language([
				'scenario' => 'index',
			]);
		}
		
		protected function init()
		{
			$this->setRelations([
				'_country'
			]);
			
			$this->addColumn((object)[
				'key' => Language::idStr(),
				'type' => 'link',
				'params' => [
					'key' => Language::idStr(),
					'route' => 'language',
					'value' => [ Language::idStr() ],
					'access' => [
						['roles' => ['admin']],
						['permissions' => ['languages.view']]
					]
				]
			]);
			
			$this->addColumn((object)[
				'key' => 'locale',
			]);
			
			$this->addColumn((object)[
				'key' => 'name',
			]);
			
			$this->addColumn((object)[
				'key' => 'country',
				'type' => 'relation',
				'params' => (object)[
					'relation' => '_country',
					'key' => Country::idStr(),
					'select' => [
						Country::idStr(),
						'name',
					],
					'value' => ['name'],
					'table' => Country::tableName(),
				],
			]);
			
			$this->addColumn((object)[
				'key' => 'status',
				'type' => 'boolean',
				'params' => [
					'true' => 'enabled',
					'false' => 'disabled'
				]
			]);
			
			$this->addColumn((object)[
				'key' => 'created_at',
				'type' => 'date',
			]);
			
			$this->addColumn((object)[
				'key' => 'updated_at',
				'type' => 'date',
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'languages.index']
				]
			];
		}
	}