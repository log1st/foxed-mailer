<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:12
	 */
	
	namespace frontend\src\table\concretes;
	
	
	use common\components\BaseActiveRecord;
	use frontend\models\SourceMessage;
	use frontend\src\table\abstractions\AbstractTable;
	
	class SourceMessageTable extends AbstractTable
	{
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getModel(): BaseActiveRecord
		{
			return new SourceMessage([
				'scenario' => 'index'
			]);
		}
		
		protected function init()
		{
			$this->setRelations([
				'_translations',
			]);
			
			$this->addColumn((object)[
				'key' => SourceMessage::idStr(),
				'type' => 'link',
				'params' => [
					'key' => SourceMessage::idStr(),
					'route' => 'source-message',
					'value' => [ SourceMessage::idStr() ],
					'access' => [
						['roles' => ['admin']],
						['permissions' => ['source-messages.view']]
					]
				]
			]);
			
			$this->addColumn((object)[
				'key' => 'category',
			]);
			
			$this->addColumn((object)[
				'key' => 'message',
			]);
			
			$this->addColumn((object)[
				'key' => 'created_at',
				'type' => 'date',
			]);
			
			$this->addColumn((object)[
				'key' => 'updated_at',
				'type' => 'date',
			]);
		}
		
		public function accessRules()
		{
			return [
				[
					'allow' => true,
					'roles' => ['admin', 'client', 'source-messages.index']
				]
			];
		}
	}