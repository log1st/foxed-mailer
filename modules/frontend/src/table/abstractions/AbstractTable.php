<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:13
	 */
	
	namespace frontend\src\table\abstractions;
	
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\src\table\requests\TableRequest;
	use Yii;
	use yii\base\InvalidConfigException;
	use yii\data\ActiveDataProvider;
	use yii\helpers\StringHelper;
	
	abstract class AbstractTable
	{
		abstract protected function getModel(): BaseActiveRecord;
		
		abstract protected function init();
		
		abstract public function accessRules();
		
		protected function getSortAttributes()
		{
			return [];
		}
		
		/** @var array */
		private $meta = [];
		
		/**
		 * @var array
		 */
		private $columns = [];
		
		/**
		 * @var BaseActiveRecord[];
		 */
		private $records = [];
		
		/**
		 * @var BaseActiveRecord|null
		 */
		private $model = null;
		
		/** @var array */
		private $relations = [];
		
		public function __construct()
		{
			$this->model = $this->getModel();
			$this->init();
		}
		
		protected function setRelations(array $relations)
		{
			$this->relations = $relations;
		}
		
		protected function addColumn(\stdClass $data)
		{
			$this->columns[] = $data;
		}
		
		protected function setMeta($key, $value)
		{
			$this->meta[$key] = $value;
		}
		
		public function getData()
		{
			$keys = array_map(function ($column) {
				return $column->key;
			}, $this->columns);
			$subKeys = array_filter(array_map(function ($column) {
				return ($column->type ?? null) === 'relation' ? $column->params->select : null;
			}, array_combine($arr = array_map(function ($column) {
				return ($column->type ?? null) === 'relation' ? $column->params->relation : $column->key;
			}, $this->columns), $this->columns)), function ($item) {
				return !empty($item);
			});
			
			return [
				'meta' => $this->meta,
				'columns' => $this->columns,
				'records' => array_map(function ($record) use ($keys, $subKeys) {
					return ArrayHelper::merge(
						array_filter($record->attributes, function ($key) use ($keys, $subKeys) {
							return in_array($key, $keys);
						}, ARRAY_FILTER_USE_KEY),
						array_map(function ($relation) use ($record, $subKeys) {
							return array_filter($record->{$relation}->attributes ?? [], function ($key) use ($relation, $subKeys) {
								return isset($subKeys[$relation]) && in_array($key, $subKeys[$relation]);
							}, ARRAY_FILTER_USE_KEY);
						}, array_combine($this->relations, $this->relations))
					);
				}, $this->records),
			];
		}
		
		public function requestRecords(TableRequest $request)
		{
			Yii::$app->request->setQueryParams([]);
			
			$this->setMeta('model', StringHelper::basename(get_class($this->model)));
			
			try {
				$query = $this->model::find()
					->with($this->relations);
				
				$provider = new ActiveDataProvider([
					'query' => $query,
					'pagination' => [
						'pageSize' => $request->limit,
						'page' => $request->page
					],
					'sort' => [
						'defaultOrder' => $request->orders,
						'attributes' => $this->getSortAttributes(),
					],
				]);
				
				
				
				$this->setMeta('total', $provider->totalCount);
				$this->setMeta('count', $provider->count);
				$this->setMeta('page', $provider->pagination->page + 1);
				$this->setMeta('pages', $provider->pagination->pageCount);
				$this->setMeta('limits', $request::LIMITS);
				$this->setMeta('limit', $request->limit);
				$this->records = $provider->getModels();
				
			} catch (InvalidConfigException $e) {
				return;
			}
		}
	}