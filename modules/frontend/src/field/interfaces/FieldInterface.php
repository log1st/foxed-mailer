<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:36
	 */
	
	namespace frontend\src\field\interfaces;
	
	
	interface FieldInterface
	{
		public function key();
		
		public function get();
		
		public function meta($key, $value);
	}