<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:37
	 */
	
	namespace frontend\src\field\concretes;
	
	
	use frontend\src\action\abstractions\AbstractField;
	
	class CheckboxList extends AbstractField
	{
		public function key()
		{
			return 'checkboxList';
		}
		
		public function options($value) {
			$this->meta['options'] = $value;
			return $this;
		}
		
		public function values($value) {
			$this->meta['values'] = $value;
			return $this;
		}
		
		public function true($value) {
			$this->meta['true'] = $value;
			return $this;
		}
		
		public function false($value) {
			$this->meta['false'] = $value;
			return $this;
		}
	}