<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:37
	 */
	
	namespace frontend\src\field\concretes;
	
	
	use frontend\src\action\abstractions\AbstractField;
	
	class Checkbox extends AbstractField
	{
		public function key()
		{
			return 'checkbox';
		}
		
		public function value($value) {
			$this->meta['value'] = $value ? 1 : 0;
			return $this;
		}
		
		public function inline($value) {
			$this->meta['inline'] = $value ? 1 : 0;
			return $this;
		}
		
		public function true($value) {
			$this->meta['true'] = $value;
			return $this;
		}
		
		public function false($value) {
			$this->meta['false'] = $value;
			return $this;
		}
	}