<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:37
	 */
	
	namespace frontend\src\field\concretes;
	
	
	use frontend\src\action\abstractions\AbstractField;
	
	class Select extends AbstractField
	{
		public function key()
		{
			return 'select';
		}
		
		public function values($value)
		{
			$this->meta['values'] = $value;
			return $this;
		}
		
		public function value($value)
		{
			$this->meta['value'] = $value;
			return $this;
		}
	}