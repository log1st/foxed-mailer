<?php
	/**
	 * Created by PhpStorm.
	 * User: log1st
	 * Date: 06.02.2018
	 * Time: 4:37
	 */
	
	namespace frontend\src\action\repositories;
	use common\helpers\UploadedFile;
	use yii\web\Request;
	
	
	/**
	 * Class RequestRepository
	 * @package frontend\src\action\repositories
	 */
	class RequestRepository
	{
		public function __construct($source)
		{
			if($source instanceof Request) {
				$this->setQuery($source->get());
				$this->setBody($source->post());
				$this->setFiles(UploadedFile::files());
			}
		}
		
		/** @var array */
		protected $query;
		/**
		 * @var
		 */
		protected $body;
		/**
		 * @var
		 */
		protected $files;
		
		/**
		 * @param $items
		 * @return $this
		 */
		public function setQuery(array $items): RequestRepository
		{
			$this->query = $items;
			return $this;
		}
		
		/**
		 * @param $items
		 * @return $this
		 */
		public function setBody(array $items): RequestRepository
		{
			$this->body = $items;
			return $this;
		}
		
		/**
		 * @param array $items
		 * @return RequestRepository
		 */
		public function setFiles(array $items): RequestRepository
		{
			$this->files = $items;
			return $this;
		}
		
		/**
		 * @param $value
		 * @return mixed|null
		 */
		public function param($value)
		{
			return $this->query[ $value ] ?? null;
		}
		
		public function field($value)
		{
			return $this->body[ $value ] ?? null;
		}
		
		public function file($value)
		{
			return $this->files[ $value ];
		}
		
		public function arguments($value = null) {
			$args = $this->__get('arguments');
			return is_null($value) ? $args : ($args[$value] ?? null);
		}
		
		public function body() {
			return $this->body;
		}
		
		public function query() {
			return $this->query;
		}
		
		public function __get($value)
		{
			return $this->param($value) ?? $this->field($value) ?? $this->file($value);
		}
	}