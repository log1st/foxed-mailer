<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\user;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\User;
	use frontend\src\action\factories\ActionFactory;
	
	/**
	 * Class Edit
	 * @package action\User
	 * @property User $model
	 *
	 */
	class Edit extends ActionFactory
	{
		
		public function getTitle(): string
		{
		return t('action', 'User.Edit.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditEmail($this->model, $this->request))->getFields(),
				(new EditPassword($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @throws \Exception
		 */
		public function handle()
		{
			$this->model->setAttributes($this->request->arguments(), false);
			
			$save = $this->model->save();
			
			if(!$save) {
				throw new \Exception('Unable to save record');
			}
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditEmail($this->model, $this->request))->rules(),
				(new EditPassword($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'User.Edit.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'User.Edit.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-pencil';
		}
	}