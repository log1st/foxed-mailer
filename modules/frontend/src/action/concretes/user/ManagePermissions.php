<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\user;
	
	
	use common\helpers\ArrayHelper;
	use common\models\auth\Assignment;
	use common\models\auth\Item;
	use frontend\models\User;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Checkbox;
	use frontend\src\field\concretes\CheckboxList;
	
	/**
	 * Class ManagePermissionsermissions
	 * @package action\message
	 * @property User $model
	 *
	 *
	 */
	class ManagePermissions extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'user.ManagePermissions.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		/**
		 * @return array
		 * @throws \yii\base\InvalidConfigException
		 */
		public function getFields(): array
		{
			return [
				(new CheckboxList())
					->options(array_map(function ($item) {
							return t('js::frontend', 'access.Role.' . $item);
						},
							Item::find()->where([
								'type' => Item::TYPE_ROLE
							])->lists(Item::idStr(), 'name'))
					)
					->values(
						Assignment::find()
							->joinWith('_item')
							->where([
								'user_id' => \Yii::$app->user->id,
								'auth_item.type' => Item::TYPE_ROLE
							])
							->lists('item_id')
					)
					->label(t('action', 'user.ManagePermissions.label.roles'))
					->name('roles'),
				(new CheckboxList())
					->options(array_map(function ($item) {
						return t('js::frontend', 'access.Permission.' . $item);
					},
						Item::find()->where([
							'type' => Item::TYPE_PERMISSION
						])->lists(Item::idStr(), 'name')))
					->values(
						Assignment::find()
							->joinWith('_item')
							->where([
								'user_id' => \Yii::$app->user->id,
								'auth_item.type' => Item::TYPE_PERMISSION
							])
							->lists('item_id')
					)
					->label(t('action', 'user.ManagePermissions.label.permissions'))
					->name('permissions'),
			];
		}
		
		/**
		 * @throws \yii\base\InvalidConfigException
		 * @throws \yii\db\Exception
		 */
		public function handle()
		{
			$exists = Item::find()->lists(Item::idStr());
			$values = [];
			$user = \Yii::$app->user->id;
			foreach ([ 'roles', 'permissions' ] as $group) {
				$values = ArrayHelper::merge($values, array_intersect($exists, $this->request->arguments($group)));
			}
			
			$values = array_map(function ($value) use ($user) {
				return [ $value, $user ];
			}, $values);
			
			$deleted = [];
			$deleting = \Yii::$app->db->createCommand()->delete(Assignment::tableName(), [
				'user_id' => $user
			], $deleted)->execute();
			$inserting = \Yii::$app->db->createCommand()->batchInsert(Assignment::tableName(), [
				'item_id', 'user_id'
			], $values)->execute();
		}
		
		public function rules(): array
		{
			return [
				[ [ 'permissions' ], 'each', 'rule' => [ 'exist', 'targetClass' => Item::className(), 'targetAttribute' => Item::idStr() ],
					'message' => t('action', 'user.ManagePermissions.rules.permissions.exist') ],
				
				[ [ 'permissions' ], 'each', 'rule' => [ 'in', 'range' => [ 0, 1 ] ],
					'message' => t('action', 'user.ManagePermissions.rules.permissions.in') ]
			];
		}
		
		public function fail(): string
		{
			return t('action', 'user.ManagePermissions.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'user.ManagePermissions.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-key';
		}
	}