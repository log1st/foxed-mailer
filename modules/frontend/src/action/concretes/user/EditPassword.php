<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\user;
	
	
	use frontend\models\User;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditPassword
	 * @package action\message
	 * @property User $model
	 *
	 *
	 */
	class EditPassword extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'user.EditPassword.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->type('password')
					->name('password')
					->label(t('action', 'user.EditPassword.label.password'))
					->placeholder(t('action', 'user.EditPassword.placeholder.password'))
			];
		}
		
		public function handle()
		{
			$this->model->password = $this->request->arguments('password');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[ [ 'password' ], 'required',
					'message' => t('action', 'user.EditPassword.rules.password.required'),
					'skipOnEmpty' => $this->model->isNewRecord ? false : true ],
				[ [ 'password' ], 'string', 'min' => 6,
					'message' => t('action', 'user.EditPassword.rules.password.min', [ 'n' => 6 ]),
					'skipOnEmpty' => $this->model->isNewRecord ? false : true ],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'user.EditPassword.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'user.EditPassword.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-key';
		}
	}