<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\user;
	
	
	use frontend\models\User;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditEmail
	 * @package action\message
	 * @property User $model
	 *
	 *
	 */
	class EditEmail extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'user.EditEmail.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('email')
					->value($this->model->email ?? null)
					->label(t('action', 'user.EditEmail.label.email'))
					->placeholder(t('action', 'user.EditEmail.placeholder.email'))
			];
		}
		
		public function handle()
		{
			$this->model->email = $this->request->arguments('email');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[ [ 'email' ], 'required',
					'message' => t('action', 'user.EditEmail.rules.email.required') ],
				
				[ [ 'email'], 'email',
					'message' => t('action', 'user.EditEmail.rules.email.email') ],
				
				[ [ 'email' ], 'unique', 'targetAttribute' => [ 'email' ],
					'message' => t('action', 'user.EditEmail.rules.email.unique') ],
			
			];
		}
		
		public function fail(): string
		{
			return t('action', 'user.EditEmail.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'user.EditEmail.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-at';
		}
	}