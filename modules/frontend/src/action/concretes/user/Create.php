<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\user;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Message;
	use frontend\models\User;
	use frontend\src\action\factories\ActionFactory;
	
	/**
	 * Class Create
	 * @package action\User
	 * @property Message $model
	 *
	 *
	 */
	class Create extends ActionFactory
	{
		
		public function getTitle(): string
		{
		return t('action', 'User.Create.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditEmail($this->model, $this->request))->getFields(),
				(new EditPassword($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @throws \Exception
		 */
		public function handle()
		{
			/** @var User $model */
			$model = new $this->model;
			
			$model->setAttributes($this->request->arguments(), false);
			
			$save = $model->save();
			
			$this->model = $model;
			
			if (!$save) {
				throw new \Exception('Unable to save record');
			}
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditEmail($this->model, $this->request))->rules(),
				(new EditPassword($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'User.Create.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'User.Create.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-plus';
		}
	}