<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\language;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Language;
	use frontend\src\action\factories\ActionFactory;
	
	/**
	 * Class Edit
	 * @package action\language
	 * @property Language $model
	 *
	 *
	 */
	class Edit extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'language.Edit.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		/**
		 * @return array
		 * @throws \yii\base\InvalidConfigException
		 */
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditLocale($this->model, $this->request))->getFields(),
				(new EditName($this->model, $this->request))->getFields(),
				(new EditStatus($this->model, $this->request))->getFields(),
				(new EditCountry($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @throws \Exception
		 */
		public function handle()
		{
			$this->model->setAttributes($this->request->arguments(), false);
			
			$save = $this->model->save();
			
			if(!$save) {
				throw new \Exception('Unable to save record');
			}
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditLocale($this->model, $this->request))->rules(),
				(new EditName($this->model, $this->request))->rules(),
				(new EditStatus($this->model, $this->request))->rules(),
				(new EditCountry($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'language.Edit.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'language.Edit.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-pencil';
		}
	}