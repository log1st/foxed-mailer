<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\language;
	
	
	use frontend\models\Language;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Checkbox;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditStatus
	 * @package action\language
	 * @property Language $model
	 *
	 *
	 */
	class EditStatus extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'language.EditStatus.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Checkbox())
					->name('status')
					->value(($this->model->status ?? null) ? 1 : 0)
					->true('enabled')
					->false('disabled')
					->label(t('action', 'language.EditStatus.label.status'))
			];
		}
		
		public function handle()
		{
			
			$this->model->status = $this->request->arguments('status') ? 1 : 0;
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[ [ 'status' ], 'required',
					'message' => t('action', 'language.EditStatus.rules.status.required') ],
				
				[ [ 'status' ], 'in', 'range' => [0, 1],
					'message' => t('action', 'language.EditStatus.rules.status.in')]
			];
		}
		
		public function fail(): string
		{
			return t('action', 'language.EditStatus.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'language.EditStatus.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-toggle-' . ($this->model->status ? 'on' : 'off');
		}
	}