<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\language;
	
	
	use frontend\models\Language;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditName
	 * @package action\language
	 * @property Language $model
	 *
	 *
	 */
	class EditName extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'language.EditName.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('name')
					->value($this->model->name ?? null)
					->label(t('action', 'language.EditName.label.name'))
					->placeholder(t('action', 'language.EditName.placeholder.name'))
			];
		}
		
		public function handle()
		{
			
			$this->model->name = $this->request->arguments('name');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[['name'], 'required',
					'message' => t('action', 'language.EditName.rules.name.required')],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'language.EditName.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'language.EditName.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-font';
		}
	}