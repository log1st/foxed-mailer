<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\language;
	
	
	use common\models\Country;
	use frontend\models\Language;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Checkbox;
	use frontend\src\field\concretes\Select;
	
	/**
	 * Class EditCountry
	 * @package action\language
	 * @property Language $model
	 *
	 *
	 */
	class EditCountry extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'language.EditCountry.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		/**
		 * @return array
		 * @throws \yii\base\InvalidConfigException
		 */
		public function getFields(): array
		{
			return [
				(new Select())
					->name('country')
					->value($this->model->country ?? null)
					->values(Country::find()->lists(Country::idStr(), 'name'))
					->label(t('action', 'language.EditCountry.label.country'))
					->placeholder(t('action', 'language.EditCountry.placeholder.country'))
			];
		}
		
		public function handle()
		{
			
			$this->model->country = $this->request->arguments('country');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[ [ 'country' ], 'required',
					'message' => t('action', 'language.EditCountry.rules.country.required') ],
				
				[ [ 'country' ], 'exist', 'targetClass' => Country::className(), 'targetAttribute' => Country::idStr(),
					'message' => t('action', 'language.EditCountry.rules.country.exist') ]
			];
		}
		
		public function fail(): string
		{
			return t('action', 'language.EditCountry.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'language.EditCountry.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-globe';
		}
	}