<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\language;
	
	
	use frontend\models\Language;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditLocale
	 * @package action\language
	 * @property Language $model
	 *
	 *
	 */
	class EditLocale extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'language.EditLocale.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('locale')
					->value($this->model->locale ?? null)
					->label(t('action', 'language.EditLocale.label.locale'))
					->placeholder(t('action', 'language.EditLocale.placeholder.locale'))
			];
		}
		
		public function handle()
		{
			
			$this->model->locale = $this->request->arguments('locale');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[['locale'], 'required',
					'message' => t('action', 'language.EditLocale.rules.locale.required')],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'language.EditLocale.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'language.EditLocale.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-code';
		}
	}