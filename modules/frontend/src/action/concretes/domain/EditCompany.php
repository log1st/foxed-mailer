<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\domain;
	
	
	use frontend\models\Company;
	use frontend\models\Domain;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Select;
	
	/**
	 * Class EditCompany
	 * @package action\message
	 * @property Domain $model
	 *
	 *
	 */
	class EditCompany extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'domain.EditCompany.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Select())
					->values(
						Company::find()->lists(Company::idStr(), 'name')
					)
					->name('company')
					->value($this->model->company ?? null)
					->label(t('action', 'domain.EditCompany.label.company'))
					->placeholder(t('action', 'domain.EditCompany.placeholder.company'))
			];
		}
		
		public function handle()
		{
			$this->model->company = $this->request->arguments('company');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[ [ 'company' ], 'required',
					'message' => t('action', 'domain.EditCompany.rules.company.required') ],
				
				[ [ 'company' ], 'exist', 'targetClass' => Company::className(), 'targetAttribute' => Company::idStr(),
					'message' => t('action', 'domain.EditCompany.rules.company.exist') ]
			];
		}
		
		public function fail(): string
		{
			return t('action', 'domain.EditCompany.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'domain.EditCompany.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-building';
		}
	}