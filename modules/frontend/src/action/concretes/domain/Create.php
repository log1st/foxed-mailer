<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\domain;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Message;
	use frontend\models\Domain;
	use frontend\src\action\factories\ActionFactory;
	
	/**
	 * Class Create
	 * @package action\Domain
	 * @property Message $model
	 *
	 *
	 */
	class Create extends ActionFactory
	{
		
		public function getTitle(): string
		{
		return t('action', 'Domain.Create.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditHost($this->model, $this->request))->getFields(),
				(new EditCompany($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @throws \Exception
		 */
		public function handle()
		{
			/** @var Domain $model */
			$model = new $this->model;
			
			$model->setAttributes($this->request->arguments(), false);
			
			$save = $model->save();
			
			$this->model = $model;
			
			if (!$save) {
				throw new \Exception('Unable to save record');
			}
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditHost($this->model, $this->request))->rules(),
				(new EditCompany($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'Domain.Create.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'Domain.Create.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-plus';
		}
	}