<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\domain;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Domain;
	use frontend\src\action\factories\ActionFactory;
	
	/**
	 * Class Edit
	 * @package action\Domain
	 * @property Domain $model
	 *
	 */
	class Edit extends ActionFactory
	{
		
		public function getTitle(): string
		{
		return t('action', 'Domain.Edit.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditHost($this->model, $this->request))->getFields(),
				(new EditCompany($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @throws \Exception
		 */
		public function handle()
		{
			$this->model->setAttributes($this->request->arguments(), false);
			
			$save = $this->model->save();
			
			if(!$save) {
				throw new \Exception('Unable to save record');
			}
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditHost($this->model, $this->request))->rules(),
				(new EditCompany($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'Domain.Edit.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'Domain.Edit.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-pencil';
		}
	}