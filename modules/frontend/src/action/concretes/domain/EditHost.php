<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\domain;
	
	
	use common\components\DomainValidator;
	use frontend\models\Domain;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditHost
	 * @package action\message
	 * @property Domain $model
	 *
	 *
	 */
	class EditHost extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'domain.EditHost.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('host')
					->value($this->model->host ?? null)
					->label(t('action', 'domain.EditHost.label.host'))
					->placeholder(t('action', 'domain.EditHost.placeholder.host'))
			];
		}
		
		public function handle()
		{
			$this->model->host = $this->request->arguments('host');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[ [ 'host' ], 'required',
					'message' => t('action', 'domain.EditHost.rules.host.required') ],
				
				[ [ 'host' ], 'url', 'enableIDN' => true, 'defaultScheme' => 'http', 'skipOnEmpty' => true,
					'message' => t('action', 'domain.EditHost.rules.host.url') ],
				
				[ [ 'host' ], DomainValidator::className(),
					'message' => t('action', 'domain.EditHost.rules.host.domain') ],
				
				[ [ 'host' ], 'unique',
					'message' => t('action', 'domain.EditHost.rules.host.unique') ],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'domain.EditHost.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'domain.EditHost.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-font';
		}
	}