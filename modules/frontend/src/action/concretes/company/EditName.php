<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\company;
	
	
	use frontend\models\Company;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditName
	 * @package action\message
	 * @property Company $model
	 *
	 *
	 */
	class EditName extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'company.EditName.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('name')
					->value($this->model->name ?? null)
					->label(t('action', 'company.EditName.label.name'))
					->placeholder(t('action', 'company.EditName.placeholder.name'))
			];
		}
		
		public function handle()
		{
			$this->model->name = $this->request->arguments('name');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[ [ 'name' ], 'required',
					'message' => t('action', 'company.EditName.rules.name.required') ],
				
				[ [ 'name'], 'string', 'min' => 4,
					'message' => t('action', 'company.EditName.rules.name.min', ['n' => 4]) ],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'company.EditName.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'company.EditName.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-font';
		}
	}