<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\company;
	
	
	use frontend\models\Company;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditToken
	 * @package action\message
	 * @property Company $model
	 *
	 *
	 */
	class EditToken extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'company.EditToken.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('token')
					->value($this->model->token ?? null)
					->label(t('action', 'company.EditToken.label.token'))
					->placeholder(t('action', 'company.EditToken.placeholder.token'))
			];
		}
		
		public function handle()
		{
			$this->model->token = $this->request->arguments('token');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
			
			];
		}
		
		public function fail(): string
		{
			return t('action', 'company.EditToken.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'company.EditToken.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-key';
		}
	}