<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\company;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Message;
	use frontend\models\Company;
	use frontend\src\action\factories\ActionFactory;
	
	/**
	 * Class Create
	 * @package action\Company
	 * @property Message $model
	 *
	 *
	 */
	class Create extends ActionFactory
	{
		
		public function getTitle(): string
		{
		return t('action', 'Company.Create.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditName($this->model, $this->request))->getFields(),
				(new EditToken($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @throws \Exception
		 */
		public function handle()
		{
			/** @var Company $model */
			$model = new $this->model;
			
			$model->setAttributes($this->request->arguments(), false);
			
			$save = $model->save();
			
			$this->model = $model;
			
			if (!$save) {
				throw new \Exception('Unable to save record');
			}
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditName($this->model, $this->request))->rules(),
				(new EditToken($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'Company.Create.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'Company.Create.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-plus';
		}
	}