<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\message;
	
	
	use common\components\BaseActiveRecord;
	use frontend\models\Language;
	use frontend\models\Message;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Select;
	
	/**
	 * Class EditLanguage
	 * @package action\message
	 * @property Message $model
	 *
	 *
	 */
	class EditLanguage extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'message.EditLanguage.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Select())
					->name('language')
					->value($this->model->language ?? null)
					->values(Language::find()->lists(Language::idStr(), true))
					->label(t('action', 'message.EditLanguage.label.language'))
					->placeholder(t('action', 'message.EditLanguage.placeholder.language'))
			];
		}
		
		public function handle()
		{
			$this->model->language = $this->request->arguments('language');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[['language'], 'required',
					'message' => t('action', 'message.EditLanguage.rules.language.required')],
				
				[['language'], 'exist', 'targetClass' => Language::className(), 'targetAttribute' => Language::idStr(),
					'message' => t('action', 'message.EditLanguage.rules.language.exists')],
				
				[['language'], 'unique', 'targetAttribute' => ['language', 'source'],
					'message' => t('action', 'message.EditLanguage.rules.language.unique')],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'message.EditLanguage.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'message.EditLanguage.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-globe';
		}
	}