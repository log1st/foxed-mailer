<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\message;
	
	
	use frontend\models\Message;
	use frontend\models\SourceMessage;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Select;
	use common\components\BaseActiveRecord;
	
	/**
	 * Class EditSource
	 * @package action\message
	 * @property Message $model
	 *
	 *
	 */
	class EditSource extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'message.EditSource.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Select())
					->name('source')
					->value($this->model->source ?? null)
					->values(SourceMessage::find()->lists(SourceMessage::idStr(), true))
					->label(t('action', 'message.EditSource.label.source'))
					->placeholder(t('action', 'message.EditSource.placeholder.source'))
			];
		}
		
		public function handle()
		{
			$this->model->source = $this->request->arguments('source');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[['source'], 'required',
					'message' => t('action', 'message.EditSource.rules.source.required')],
				
				[['source'], 'exist', 'targetClass' => SourceMessage::className(), 'targetAttribute' => SourceMessage::idStr(),
					'message' => t('action', 'message.EditSource.rules.source.exists')],
				
				[['source'], 'unique', 'targetAttribute' => ['source', 'language'],
					'message' => t('action', 'message.EditSource.rules.source.unique')],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'message.EditSource.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'message.EditSource.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-code';
		}
	}