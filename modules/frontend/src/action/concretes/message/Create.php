<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\message;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Language;
	use frontend\models\Message;
	use frontend\models\SourceMessage;
	use frontend\src\action\factories\ActionFactory;
	use common\components\BaseActiveRecord;
	use frontend\src\action\helpers\MessageHelper;
	use yii\db\Expression;
	
	/**
	 * Class Create
	 * @package action\message
	 * @property Message $model
	 *
	 *
	 */
	class Create extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'message.Create.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditSource($this->model))->getFields(),
				(new EditLanguage($this->model))->getFields(),
				(new EditTranslation($this->model))->getFields()
			);
		}
		
		/**
		 * @return mixed|array
		 * @throws \Exception
		 */
		public function handle()
		{
			/** @var Message $model */
			$model = new $this->model;
			
			$model->setAttributes($this->request->arguments(), false);
			
			$save = $model->save();
			
			$this->model = $model;
			
			if (!$save) {
				throw new \Exception('Unable to save record');
			}
			
			return MessageHelper::afterSaveCommands($model);
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditSource($this->model))->rules(),
				(new EditLanguage($this->model))->rules(),
				(new EditTranslation($this->model))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'message.Create.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'message.Create.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-plus';
		}
	}