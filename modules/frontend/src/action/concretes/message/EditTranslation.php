<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\message;
	
	
	use frontend\models\Message;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	use common\components\BaseActiveRecord;
	
	/**
	 * Class EditTranslation
	 * @package action\message
	 * @property Message $model
	 *
	 *
	 */
	class EditTranslation extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'message.EditTranslation.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('translation')
					->value($this->model->translation ?? null)
					->label(t('action', 'message.EditTranslation.label.translation'))
					->placeholder(t('action', 'message.EditTranslation.placeholder.translation'))
			];
		}
		
		public function handle()
		{
			$this->model->translation = $this->request->arguments('translation');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[['translation'], 'required',
					'message' => t('action', 'message.EditTranslation.rules.translation.required')],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'message.EditTranslation.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'message.EditTranslation.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-language';
		}
	}