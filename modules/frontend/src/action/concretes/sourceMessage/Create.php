<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\sourceMessage;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Message;
	use frontend\models\SourceMessage;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\action\helpers\MessageHelper;
	
	/**
	 * Class Create
	 * @package action\sourceMessage
	 * @property Message $model
	 *
	 *
	 */
	class Create extends ActionFactory
	{
		
		public function getTitle(): string
		{
		return t('action', 'sourceMessage.Create.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditCategory($this->model, $this->request))->getFields(),
				(new EditMessage($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @return mixed|array
		 * @throws \Exception
		 */
		public function handle()
		{
			/** @var SourceMessage $model */
			$model = new $this->model;
			
			$model->setAttributes($this->request->arguments(), false);
			
			$save = $model->save();
			
			$this->model = $model;
			
			if (!$save) {
				throw new \Exception('Unable to save record');
			}
			
			return MessageHelper::afterSaveCommands($model);
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditCategory($this->model, $this->request))->rules(),
				(new EditMessage($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'sourceMessage.Create.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'sourceMessage.Create.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-plus';
		}
	}