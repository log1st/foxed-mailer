<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\sourceMessage;
	
	
	use common\helpers\ArrayHelper;
	use frontend\models\Message;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\action\helpers\MessageHelper;
	
	/**
	 * Class Edit
	 * @package action\sourceMessage
	 * @property Message $model
	 *
	 *
	 */
	class Edit extends ActionFactory
	{
		
		public function getTitle(): string
		{
		return t('action', 'sourceMessage.Edit.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return ArrayHelper::merge(
				(new EditCategory($this->model, $this->request))->getFields(),
				(new EditMessage($this->model, $this->request))->getFields()
			);
		}
		
		/**
		 * @return mixed|array
		 * @throws \Exception
		 */
		public function handle()
		{
			$this->model->setAttributes($this->request->arguments(), false);
			
			$save = $this->model->save();
			
			if(!$save) {
				throw new \Exception('Unable to save record');
			}
			
			return MessageHelper::afterSaveCommands($this->model);
		}
		
		public function rules(): array
		{
			return ArrayHelper::merge(
				(new EditCategory($this->model, $this->request))->rules(),
				(new EditMessage($this->model, $this->request))->rules()
			);
		}
		
		public function fail(): string
		{
			return t('action', 'sourceMessage.Edit.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'sourceMessage.Edit.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-pencil';
		}
	}