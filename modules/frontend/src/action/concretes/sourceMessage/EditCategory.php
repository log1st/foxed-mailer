<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\sourceMessage;
	
	
	use frontend\models\SourceMessage;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	
	/**
	 * Class EditCategory
	 * @package action\message
	 * @property SourceMessage $model
	 *
	 *
	 */
	class EditCategory extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'message.EditCategory.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('category')
					->value($this->model->category ?? null)
					->label(t('action', 'message.EditCategory.label.category'))
					->placeholder(t('action', 'message.EditCategory.placeholder.category'))
			];
		}
		
		public function handle()
		{
			$this->model->category = $this->request->arguments('category');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[['category'], 'required',
					'message' => t('action', 'message.EditCategory.rules.category.required')],
				
				[['category'], 'unique', 'targetAttribute' => ['category', 'message'],
					'message' => t('action', 'message.EditCategory.rules.category.unique')],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'message.EditCategory.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'message.EditCategory.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-code';
		}
	}