<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:35
	 */
	
	namespace action\sourceMessage;
	
	
	use frontend\models\Message;
	use frontend\models\SourceMessage;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\field\concretes\Text;
	use common\components\BaseActiveRecord;
	
	/**
	 * Class EditMessage
	 * @package action\message
	 * @property SourceMessage $model
	 *
	 *
	 */
	class EditMessage extends ActionFactory
	{
		
		public function getTitle(): string
		{
			return t('action', 'message.EditMessage.title');
		}
		
		public function getSubTitle()
		{
			return true;
		}
		
		public function getFields(): array
		{
			return [
				(new Text())
					->name('message')
					->value($this->model->message ?? null)
					->label(t('action', 'message.EditMessage.label.message'))
					->placeholder(t('action', 'message.EditMessage.placeholder.message'))
			];
		}
		
		public function handle()
		{
			$this->model->message = $this->request->arguments('message');
			$this->model->save(false);
		}
		
		public function rules(): array
		{
			return [
				[['message'], 'required',
					'message' => t('action', 'message.EditMessage.rules.message.required')],
				
				[['message'], 'unique', 'targetAttribute' => ['message', 'category'],
					'message' => t('action', 'message.EditMessage.rules.message.unique')],
			];
		}
		
		public function fail(): string
		{
			return t('action', 'message.EditMessage.fail', $this->model->attributes);
		}
		
		public function succeed(): string
		{
			return t('action', 'message.EditMessage.succeed', $this->model->attributes);
		}
		
		public function getIcon(): string
		{
			return 'fa fa-font';
		}
	}