<?php
	/**
	 * Created by PhpStorm.
	 * User: log1st
	 * Date: 04.02.2018
	 * Time: 8:29
	 */
	
	namespace frontend\src\action\helpers;
	
	
	use action\message\Create;
	use frontend\models\Language;
	use frontend\models\SourceMessage;
	use yii\db\Expression;
	
	class MessageHelper
	{
		public static function afterSaveCommands($model)
		{
			$languages = Language::find()->where([ 'status' => Language::STATUS_ENABLED ])->lists(Language::idStr());
			$query = SourceMessage::find()
				->select([
					SourceMessage::idStr(),
					new Expression('GROUP_CONCAT(messages.language SEPARATOR \',\') as mLanguages'),
					new Expression('COUNT(messages.message_id) as mCount')
				])
				->joinWith('_translations')
				->groupBy(SourceMessage::idStr())
				->having([
					'<', 'mCount', count($languages)
				]);
			
			if (!$query->count()) {
				return [
					[
						'type' => 'record',
						'meta' => [
							'model' => 'message',
							'id' => $model->id
						]
					]
				];
			}
			
			$source = $query->asArray()->one();
			
			$langs = explode(',', $source[ 'mLanguages' ]);
			
			foreach($langs as $lang) {
				if(in_array($lang, $languages)) {
					if (($key = array_search($lang, $languages)) !== false) {
						unset($languages[$key]);
					}
				}
			}
			
			return [
				'messages',
				[
					'type' => 'list',
					'meta' => [
						'model' => 'messages',
					]
				],
				[
					'type' => 'action',
					'meta' => [
						'name' => Create::class,
						'fields' => [
							'source' => $source[ 'source_id' ],
							'language' => $languages[0]
						]
					]
				]
			];
		}
	}