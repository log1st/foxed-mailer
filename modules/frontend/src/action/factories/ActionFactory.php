<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:20
	 */
	
	namespace frontend\src\action\factories;
	
	
	use common\components\BaseActiveRecord;
	use frontend\src\action\repositories\RequestRepository;
	use frontend\src\field\interfaces\FieldInterface;
	use yii\base\DynamicModel;
	use yii\base\InvalidConfigException;
	use yii\validators\Validator;
	use yii\web\Request;
	
	abstract class ActionFactory implements \JsonSerializable
	{
		/** @var BaseActiveRecord */
		protected $model;
		
		/** @var RequestRepository */
		protected $request;
		
		/**
		 * Title of form.
		 * @return string
		 */
		abstract public function getTitle(): string;
		
		/**
		 * Subtitle of form. May contain description or be null to be not displayed
		 * @return string|null
		 */
		abstract public function getSubTitle();
		
		/**
		 * Array of fields
		 * @return FieldInterface[]
		 */
		abstract public function getFields(): array;
		
		/**
		 * Handler
		 */
		abstract public function handle();
		
		/**
		 * Array of Yii2 Validator rules
		 * @return array
		 */
		abstract public function rules(): array;
		
		/**
		 * Fail message
		 * @return string
		 */
		abstract public function fail(): string;
		
		/**
		 * Success message
		 * @return string
		 */
		abstract public function succeed(): string;
		
		/**
		 * Validate
		 *
		 * @return bool
		 * @throws InvalidConfigException
		 */
		public function validate()
		{
			$validators = $this->model->getValidators();
			foreach ($this->rules() as $rule) {
				if ($rule instanceof Validator) {
					$validators->append($rule);
				} elseif (is_array($rule) && isset($rule[ 0 ], $rule[ 1 ])) {
					$validator = Validator::createValidator($rule[ 1 ], $this->model, (array)$rule[ 0 ], array_slice($rule, 2));
					$validators->append($validator);
				} else {
					throw new InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
				}
			}
			$this->model->setAttributes($this->request->arguments(), false);
			return $this->model->validate();
		}
		
		/**
		 * ActionFactory constructor.
		 * @param BaseActiveRecord|null $record
		 * @param RequestRepository     $request
		 */
		public function __construct(BaseActiveRecord $record = null, RequestRepository $request = null)
		{
			$this->model = $record;
			$this->request = $request;
		}
		
		/**
		 * @return string
		 */
		public function getIcon(): string
		{
			return '';
		}
		
		/**
		 * @return FieldInterface[]
		 */
		final public function buildForm()
		{
			return $this->getFields();
		}
		
		/**
		 * @return BaseActiveRecord|null
		 */
		public function getRecord()
		{
			return $this->model;
		}
		
		/**
		 * @return array|mixed
		 */
		final public function jsonSerialize()
		{
			$title = $this->getTitle();
			
			$subTitle = $this->getSubTitle();
			if ($subTitle === true) {
				$subTitle = t('model.' . $this->model::shortClassName(), 'title');
			}
			
			$fields = $this->buildForm();
			
			if ($this->model) {
				foreach ($fields as $field) {
					$field->meta('class', $this->model::shortClassName());
				}
			}
			
			$icon = $this->getIcon();
			
			return [
				'record' => $this->model->id ?? '',
				'action' => get_class($this),
				'title' => $title,
				'subTitle' => $subTitle,
				'fields' => $fields,
				'icon' => $icon
			];
		}
	}