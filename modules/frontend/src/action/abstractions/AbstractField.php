<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 18.01.2018
	 * Time: 4:39
	 */
	
	namespace frontend\src\action\abstractions;
	
	
	use frontend\src\field\interfaces\FieldInterface;
	
	abstract class AbstractField implements FieldInterface, \JsonSerializable
	{
		protected $meta = [];
		
		public function __construct()
		{
			return $this;
		}
		
		final public function placeholder($value)
		{
			$this->meta[ 'placeholder' ] = $value;
			return $this;
		}
		
		final public function label($value)
		{
			$this->meta[ 'label' ] = $value;
			return $this;
		}
		
		final public function name($value)
		{
			$this->meta[ 'name' ] = "arguments[$value]";
			$this->meta[ 'nameClear' ] = $value;
			return $this;
		}
		
		final public function get()
		{
			return (object)[
				'key' => $this->key(),
				'meta' => $this->meta,
				'id' => uniqid()
			];
		}
		
		public function meta($key, $value)
		{
			$this->meta[ $key ] = $value;
		}
		
		public function jsonSerialize()
		{
			return $this->get();
		}
	}