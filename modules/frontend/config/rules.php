<?php
	
	return [
		'/' => 'site/index',
		'POST messages' => 'rest/message',
		'messages' => 'rest/messages',
		
		
		'<action:(get-user|get-bar|documentation)>' => 'rest/<action>',
		'<action:(sign-in|sign-out)>' => 'auth/<action>',
		'table/<key:(companies|accounts|domains|source-messages|messages|languages|users)>' => 'rest/table',
		'view/<key:user|language|message|source-message|domain|account|company>/<id:\d+>' => 'rest/view',
		'action' => 'rest/action'
	];