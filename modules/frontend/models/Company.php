<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:33
	 */
	
	namespace frontend\models;
	
	
	use common\models\User;
	use http\Exception\BadQueryStringException;
	
	class Company extends \common\models\Company
	{
		public static function find() {
			$query = parent::find();
			$user = \Yii::$app->user;
			
			if($user->can('admin')) {
				return $query;
			}
			
			if($user->can('client')) {
				return $query->where([
					'user' => $user->id
				]);
			}
			
			if($user->can('manager') || $user->can('developer')) {
				/** @var User $identity */
				$identity = $user->identity;
				return $query->where([
					'user' => $identity->parent
				]);
			}
			
			throw new BadQueryStringException(t(['model.Company'], 'find.badQuery'));
		}
	}