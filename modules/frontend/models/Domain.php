<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 9:33
	 */
	
	namespace frontend\models;
	
	
	use common\models\User;
	use http\Exception\BadQueryStringException;
	
	class Domain extends \common\models\Domain
	{
		public static function find() {
			$query = parent::find();
			$user = \Yii::$app->user;
			
			if($user->can('admin')) {
				return $query;
			}
			
			if($user->can('client')) {
				return $query->where([
					'in', 'company', Company::find()->where(['user' => $user->id])->select([Company::idStr()])
				]);
			}
			
			if($user->can('manager') || $user->can('developer')) {
				/** @var User $identity */
				$identity = $user->identity;
				return $query->where([
					'in', 'company', Company::find()->where(['user' => $identity->parent])->select([Company::idStr()])
				]);
			}
			
			throw new BadQueryStringException(t(['model.Domain'], 'find.badQuery'));
		}
	}