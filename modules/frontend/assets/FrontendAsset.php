<?php
	
	
	namespace frontend\assets;
	use sweelix\webpack\WebpackAssetBundle;
	
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 12:18
	 */
	
	class FrontendAsset extends WebpackAssetBundle
	{
		public $cacheEnabled = YII_CACHE;
		public $cache = 'cache';
		
		public $webpackPath = '@webroot';
		
		public $webpackAssetCatalog = 'webpack.json';
		
		public $webpackBundles = [
			'common',
		];
	}