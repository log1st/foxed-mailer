'use strict';

import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'index',
			component: r => require.ensure([], () => r(require('../templates/pages/Index/Index.vue')))
		},
		{
			path: '/documentation',
			name: 'documentation',
			component: r => require.ensure([], () => r(require('../templates/pages/Documentation/Documentation.vue')))
		},
		{
			path: '/sign-in',
			name: 'sign-in',
			component: r => require.ensure([], () => r(require('../templates/pages/SignIn/SignIn.vue')))
		},
		{
			path: '/table/:key',
			name: 'table',
			component: r => require.ensure([], () => r(require('../templates/pages/List/List.vue')))
		},
		{
			path: '/view/company/:id',
			name: 'company',
			component: r => require.ensure([], () => r(require('../templates/pages/View/Company/Company.vue')))
		},
		{
			path: '/view/account/:id',
			name: 'account',
			component: r => require.ensure([], () => r(require('../templates/pages/View/Account/Account.vue')))
		},
		
		{
			path: '/view/domain/:id',
			name: 'domain',
			component: r => require.ensure([], () => r(require('../templates/pages/View/Domain/Domain.vue')))
		},
		
		{
			path: '/view/source-message/:id',
			name: 'source-message',
			component: r => require.ensure([], () => r(require('../templates/pages/View/SourceMessage/SourceMessage.vue')))
		},
		{
			path: '/view/message/:id',
			name: 'message',
			component: r => require.ensure([], () => r(require('../templates/pages/View/Message/Message.vue')))
		},
		
		{
			path: '/view/language/:id',
			name: 'language',
			component: r => require.ensure([], () => r(require('../templates/pages/View/Language/Language.vue')))
		},
		{
			path: '/view/user/:id',
			name: 'user',
			component: r => require.ensure([], () => r(require('../templates/pages/View/User/User.vue')))
		},
		
		
		// Rest
		{
			path: '/get-user',
			name: 'get-user',
		},
		{
			path: '/get-bar',
			name: 'get-bar'
		},
		{
			path: '/messages',
			name: 'i18n-messages'
		},
		{
			path: '/sign-out',
			name: 'sign-out'
		},
		{
			path: '/action',
			name: 'action'
		}
	]
});

export default router;