'use strict';

import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		navItems: [
			{
				name: 'documentation',
				access: [
					{roles: ['admin', 'client']},
					{roles: ['manager', 'developer'], permissions: ['documentation']}
				]
			}
		],
		messages: [],
		user: null,
		xhrs: [],
		table: {}
	},
	mutations: {
		setMessages: (state, messages) => {
			state.messages = messages;
		},
		setUser: (state, user) => {
			state.user = user;
		},
	},
	getters: {
		navigation: state => state.navItems.map(item => {
			return {
				name: item.name,
				title: item.title || null,
				access: item.access
			}
		}),
		
	}
});

export default store;