import MessageFormat from 'messageformat'

export default class CustomFormatter {
	constructor (options = {}) {
		let mf = new MessageFormat(this._locale);
		
		mf.addFormatters({
			toUpper: v => v.toUpperCase(),
			toLower: v => v.toLowerCase(),
			ucFirst: v => v.charAt(0).toUpperCase() + v.slice(1)
		});
		
		this._locale = options.locale || 'en';
		this._formatter = mf;
		this._formatter.setIntlSupport(true);
		this._caches = Object.create(null)
	}
	
	interpolate (message, values) {
		let fn = this._caches[message];
		if (!fn) {
			fn = this._formatter.compile(message, this._locale);
			this._caches[message] = fn
		}
		return [fn(values)]
	}
}