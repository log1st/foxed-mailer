'use strict';

import Vue from 'vue';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);

import store from 'resources/store';
import router from 'resources/router';
import Ajax from 'classes/Ajax';
import Formatter from './i18n/formatter';

let formatter = new Formatter({
	locale: document.querySelector('html').dataset.lang
});

const i18n = new VueI18n({
	formatter,
	messages: store.state.messages,
	fallbackLocale: 'en',
	missing: (locale, key, vm) => {
		(new Ajax({
			url: router.resolve({
				name: 'i18n-messages'
			}).href,
			dataType: 'json',
			data: {
				key: key,
				category: 'frontend'
			},
			type: 'post'
		}))
			.then((json) => {
			
			})
			.catch((res) => {
			
			});
	}
});

export default i18n;