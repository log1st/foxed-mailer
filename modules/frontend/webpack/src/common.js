import './third-party/select2';
import 'tooltip.js';

import Vue from 'vue';
import moment from 'moment';

import Vue2Filters from 'vue2-filters'
import Ajax from 'classes/Ajax';

import App from './templates/App.vue';


moment.locale(
	document.querySelector('html').dataset.lang
);

Vue.use(Vue2Filters);


let Application,
	router = require('resources/router.js').default,
	store = require('resources/store.js').default,
	InitApp = (options = {}) => {
		store.commit('setMessages', options.messages || []);
		let i18n = require('resources/i18n.js').default;
		
		i18n.locale = options.locale || document.querySelector('html').dataset.lang || 'en';
		
		Application = new Vue({
			el: '#app',
			
			template: '<App/>',
			components: {
				App
			},
			store,
			i18n,
			router
		}).$mount('#app');
		
		window.vue = Application;
	};


let request = new Ajax({
	url: router.resolve({
		name: 'i18n-messages'
	}).href,
	dataType: 'json',
	data: {
		key: 'frontend'
	}
});

request
	.then((json) => {
		if (json['status'] === 200) {
			InitApp(json['data']);
		} else {
			InitApp();
		}
	})
	.catch(() => {
		InitApp();
	});


export default Application;