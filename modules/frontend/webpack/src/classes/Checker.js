'use strict';

import store from 'resources/store';

class Checker {
	
	static is(role) {
		return store.state.user && store.state.user.roles.indexOf(role) > -1;
	}
	
	static can(permission) {
		return store.state.user && store.state.user.permissions.indexOf(permission) > -1;
	}
	
	
	static byConfig(access) {
		if (typeof access === 'undefined') {
			return true;
		}
		
		return access.filter(group => {
				let roles = typeof group.permissions === 'undefined' || group.permissions.length === 0 || group.permissions.filter((subItem) => {
						return Checker.can(subItem);
					}).length > 0,
					permissions = (
						typeof group.roles === 'undefined' || group.roles.length === 0 || group.roles.filter(function (subItem) {
							return Checker.is(subItem);
						}).length > 0
					);
				return roles && permissions;
			}
		).length > 0;
	}
	
}

export default Checker;