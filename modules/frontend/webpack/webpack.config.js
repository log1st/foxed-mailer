'use strict';

const
	path = require('path'),
	webpack = require('webpack'),
	src = path.resolve(__dirname, './src'),
	dist = path.resolve(__dirname, '../web/dist'),
	port = 8081,
	isProduction = process.env.NODE_ENV === 'production',
	publicPath = isProduction ? '/dist/' : 'http://127.0.0.1:' + port + '/',
	
	AssetsPlugin = require('assets-webpack-plugin'),
	ExtractTextPlugin = require('extract-text-webpack-plugin'),
	extractSass = new ExtractTextPlugin({
		filename: "[name].[contenthash].css",
		disable: !isProduction,
		allChunks: true
	}),
	UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
	
	styleLoader = {
		test: /\.css$/,
		use: [
			{
				loader: "css-loader",
				options: {
					minimize: isProduction,
					sourceMap: !isProduction
				}
			}
		]
	},
	scssLoader = {
		test: /\.scss$/,
		use: [
			{
				loader: "css-loader",
				options: {
					minimize: isProduction,
					sourceMap: !isProduction
				}
			},
			{
				loader: 'resolve-url-loader',
				options: {
					sourceMap: !isProduction
				}
			}, {
				loader: 'postcss-loader',
				options: {
					sourceMap: !isProduction
				}
			}, {
				loader: "sass-loader",
				options: {
					sourceMap: !isProduction
				}
			}
		]
	};

if (isProduction) {
	styleLoader.use = ExtractTextPlugin.extract({
		use: styleLoader.use,
		fallback: 'style-loader'
	});
	scssLoader.use = extractSass.extract({
		use: scssLoader.use
	});
} else {
	styleLoader.use.unshift({
		loader: "style-loader"
	});
	scssLoader.use.unshift({
		loader: "style-loader"
	});
}

let alias = {
	'vue$': 'vue/dist/vue.' + (isProduction ? 'min' : 'esm') + '.js',
	'images': path.resolve(src, 'images'),
	'styles': path.resolve(src, 'styles'),
	'fonts': path.resolve(src, 'fonts'),
	'partials': path.resolve(src, 'templates/partials'),
	'classes': path.resolve(src, 'classes'),
	'resources': path.resolve(src, 'resources'),
};

const config = {
	context: src,
	entry: {
		common: ['babel-polyfill', './common']
	},
	resolve: {
		extensions: ['*', '.es6', '.js', '.scss', '/index.es6', '/index.js', '/index.scss', '.vue'],
		alias
	},
	resolveLoader: {
		alias
	},
	output: {
		library: "[name]",
		path: dist,
		filename: '[name].[hash].js',
		publicPath: publicPath
	},
	module: {
		rules: [
			
			{
				test: /\.vue$/,
				exclude: /(node_modules|bower_components)/,
				use: [
					{
						loader: 'babel-loader',
					},
					'vue-loader',
				]
			},
			{
				test: /\.(js|es6)$/,
				exclude: [/node_modules/],
				use: [{
					loader: 'babel-loader',
				}]
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg|otf)([?\w.#_=]+)?$/,
				exclude: path.resolve(src, 'images/'),
				loader: 'file-loader'
			},
			{
				test: /\.(svg)$/,
				loader: 'vue-svg-loader',
				include: path.resolve(src, 'images/svg'),
				options: {
					svgo: {
						plugins: [
							{
								removeDoctype: true
							},
							{
								removeComments: true
							},
							{
								cleanupIDs: false
							},
						]
					}
				}
			},
			{
				test: /\.(jpg|jpeg|png|gif|ico)$/,
				loader: 'file-loader'
			}
		]
	},
	plugins: [
		new AssetsPlugin({
			path: dist + '/../',
			filename: 'webpack.json',
			prettyPrint: isProduction
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			Popper: ['popper.js', 'default'],
			"window.Tether": 'tether'
		})
	]
};

if (!isProduction) {
	config.devtool = 'inline-source-map';
	config.watch = true;
	config.devServer = {
		port: port,
		hot: true,
		contentBase: dist,
		publicPath: config.output.publicPath,
		headers: {"Access-Control-Allow-Origin": "*"}
	};
	config.plugins.push(new webpack.HotModuleReplacementPlugin());
} else {
	config.plugins.push(extractSass);
}
config.module.rules.push(styleLoader);
config.module.rules.push(scssLoader);

module.exports = config;
