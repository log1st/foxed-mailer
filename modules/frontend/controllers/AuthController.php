<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:25
	 */
	
	namespace frontend\controllers;
	
	
	use frontend\components\BaseJsonFrontendController;
	use frontend\controllers\actions\auth\SignInAction;
	use frontend\controllers\actions\auth\SignOutAction;
	
	class AuthController extends BaseJsonFrontendController
	{
		public function actions() {
			return [
				'sign-in' => [
					'class' => SignInAction::className()
				],
				'sign-out' => [
					'class' => SignOutAction::className()
				]
			];
		}
	}