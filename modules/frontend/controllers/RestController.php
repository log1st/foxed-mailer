<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 14:06
	 */
	
	namespace frontend\controllers;
	
	
	use frontend\components\BaseJsonFrontendController;
	use frontend\controllers\actions\rest\ActionAction;
	use frontend\controllers\actions\rest\GetBarAction;
	use frontend\controllers\actions\rest\GetUserAction;
	use frontend\controllers\actions\rest\MessagesAction;
	use frontend\controllers\actions\rest\TableAction;
	use frontend\controllers\actions\rest\ViewAction;
	use frontend\controllers\actions\site\InsertMessageAction;
	use Yii;
	use yii\filters\AccessControl;
	use yii\helpers\ArrayHelper;
	use yii\web\HttpException;
	
	class RestController extends BaseJsonFrontendController
	{
		
		public function behaviors()
		{
			return [
				'access' => [
					'class' => AccessControl::className(),
					'only' => ['get-user', 'table', 'view'],
					'rules' => [
						[
							'allow' => true,
							'actions' => ['get-user', 'table', 'view'],
							'roles' => ['@'],
						],
						[
							'allow' => false,
						],
					],
					'denyCallback' => function ($rule, $action) {
						return $this->module->runAction('error/index', [
							'code' => 403,
							'message' => t('controller', 'rest._behaviors.noAccess')
						]);
					},
				],
			];
		}
		
		public function actions()
		{
			return [
				'messages' => [
					'class' => MessagesAction::className(),
				],
				'message' => [
					'class' => InsertMessageAction::className(),
				],
				'get-user' => [
					'class' => GetUserAction::className(),
				],
				'table' => [
					'class' => TableAction::className(),
				],
				'get-bar' => [
					'class' => GetBarAction::className(),
				],
				'view' => [
					'class' => ViewAction::className(),
				],
				'action' => [
					'class' => ActionAction::className()
				]
			];
		}
	}