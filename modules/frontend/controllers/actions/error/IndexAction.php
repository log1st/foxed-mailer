<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 11.01.2018
	 * Time: 4:22
	 */
	
	namespace frontend\controllers\actions\error;
	
	
	use frontend\controllers\ErrorController;
	use yii\base\Action;
	use Yii;
	use yii\web\HttpException;
	use yii\web\Response;
	
	/**
	 * Class IndexAction
	 * @package frontend\controllers\actions\error
	 *
	 * @property ErrorController $controller
	 */
	class IndexAction extends Action
	{
		public function run($code = null, $message = null) {
			/** @var HttpException $exception */
			$exception = Yii::$app->errorHandler->exception;
			if(\Yii::$app->request->headers->get('Ajax') === '1') {
				\Yii::$app->response->format = Response::FORMAT_JSON;
				return $this->controller->response($code ?? $exception->statusCode, [
					'message' => $message ?? $exception->getMessage()
				]);
			}
			return $this->controller->renderPartial('//layouts/spa');
		}
	}