<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:27
	 */
	
	namespace frontend\controllers\actions\auth;
	
	
	use frontend\controllers\AuthController;
	use frontend\forms\SignInForm;
	use Yii;
	use yii\base\Action;
	
	/**
	 * Class SignInAction
	 * @package frontend\controllers\actions\auth
	 *
	 * @property AuthController $controller
	 */
	class SignInAction extends Action
	{
		public function run() {
			$form = new SignInForm([
				'scenario' => 'sign-in'
			]);
			
			$loaded = $form->load(Yii::$app->request->post());
			
			if(!$loaded) {
				return $this->controller->response(400, [
					'message' => t('controller', 'auth.SignIn.error.loading')
				]);
			}
			
			$validated = $form->validate();
			
			if(!$validated) {
				return $this->controller->response(400, [
					'errors' => $form->errors,
//					'message' => t('controller', 'auth.SignIn.error.validating')
				]);
			}
			
			$user = $form->getUser();
			
			$logged = Yii::$app->user->login($user, $form->remember ? (10 * 365 * 24 * 60 * 60) : 0);
			
			if(!$logged) {
				return $this->controller->response(400, [
					'message' => t('controller', 'auth.SignIn.error.logging')
				]);
			}
			
			return $this->controller->response(200, [
				'message' => t('controller', 'auth.SignIn.success')
			]);
		}
	}