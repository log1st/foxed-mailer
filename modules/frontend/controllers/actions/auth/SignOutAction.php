<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:27
	 */
	
	namespace frontend\controllers\actions\auth;
	
	
	use frontend\controllers\AuthController;
	use frontend\forms\SignInForm;
	use Yii;
	use yii\base\Action;
	
	/**
	 * Class SignOutAction
	 * @package frontend\controllers\actions\auth
	 *
	 * @property AuthController $controller
	 */
	class SignOutAction extends Action
	{
		public function run() {
			
			if(\Yii::$app->user->isGuest) {
				return $this->controller->response(400, [
					'message' => t('controller', 'auth.SignOut.error.already')
				]);
			}
			
			$logout = \Yii::$app->user->logout();
			
			if(!$logout) {
				return $this->controller->response(500, [
					'message' => t('controller', 'auth.SignOut.error.logouting')
				]);
			}
			
			return $this->controller->response(200, [
				'message' => t('controller', 'auth.SignOut.success')
			]);
		}
	}