<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 16:58
	 */
	
	namespace frontend\controllers\actions\site;
	
	
	use yii\base\Action;
	
	class InsertMessageAction extends Action
	{
		public function run() {
			$r = \Yii::$app->request;
			t('js::' . $r->post('category'), $r->post('key'));
			
			return $this->controller->response(200);
		}
	}