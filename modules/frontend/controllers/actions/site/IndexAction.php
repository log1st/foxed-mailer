<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 12:22
	 */
	
	namespace frontend\controllers\actions\site;
	
	
	use frontend\components\BaseFrontendController;
	use yii\base\Action;
	
	/**
	 * Class IndexAction
	 * @package frontend\controllers\actions\site
	 *
	 * @property BaseFrontendController $controller
	 */
	class IndexAction extends Action
	{
		public function run() {
			return $this->controller->renderPartial('//layouts/spa');
		}
	}