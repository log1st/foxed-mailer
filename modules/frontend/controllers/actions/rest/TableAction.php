<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 14:07
	 */
	
	namespace frontend\controllers\actions\rest;
	
	
	use common\helpers\ArrayHelper;
	use frontend\controllers\RestController;
	use frontend\src\table\abstractions\AbstractTable;
	use frontend\src\table\concretes\AccountTable;
	use frontend\src\table\concretes\CompanyTable;
	use frontend\src\table\concretes\DomainTable;
	use frontend\src\table\concretes\LanguageTable;
	use frontend\src\table\concretes\MessageTable;
	use frontend\src\table\concretes\SourceMessageTable;
	use frontend\src\table\concretes\UserTable;
	use frontend\src\table\requests\TableRequest;
	use Yii;
	use yii\base\Action;
	use yii\filters\AccessControl;
	use yii\web\ForbiddenHttpException;
	
	/**
	 * Class TableAction
	 *
	 * @property RestController $controller
	 */
	class TableAction extends Action
	{
		protected $models = [
			'companies' => CompanyTable::class,
			'accounts' => AccountTable::class,
			'domains' => DomainTable::class,
			'source-messages' => SourceMessageTable::class,
			'messages' => MessageTable::class,
			'languages' => LanguageTable::class,
			'users' => UserTable::class
		];
		
		public function run($key)
		{
			if(!in_array($key, array_keys($this->models))) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.table.error.unknownModel')
				]);
			}
			
			$table = $this->models[$key];
			/** @var AbstractTable $table */
			$table = new $table;
			
			try {
				$ruler = new AccessControl([
					'rules' => $table->accessRules()
				]);
				$ruler->beforeAction($this);
			}   catch (ForbiddenHttpException $e) {
				return $this->controller->module->runAction('error/index', [
					'code' => 403,
					'message' => t('controller', 'rest._behaviors.noAccess')
				]);
			}
			
			$request = new TableRequest([
				'scenario' => 'index'
			]);
			$request->load(Yii::$app->request->get(), '');
			$request->validate();
			
			$table->requestRecords($request);
			
			$data = $table->getData();
			
			return $this->controller->response(200, $data);
		}
	}