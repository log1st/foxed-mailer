<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 14:07
	 */
	
	namespace frontend\controllers\actions\rest;
	
	use common\components\BaseActiveRecord;
	use frontend\models\Account;
	use frontend\models\Domain;
	use frontend\models\Language;
	use frontend\models\Message;
	use frontend\models\SourceMessage;
	use frontend\models\User;
	use frontend\controllers\RestController;
	use frontend\models\Company;
	use frontend\src\transform\AbstractTransformer;
	use frontend\src\transform\transformers\AccountTransformer;
	use frontend\src\transform\transformers\CompanyTransformer;
	use frontend\src\transform\transformers\DomainTransformer;
	use frontend\src\transform\transformers\LanguageTransformer;
	use frontend\src\transform\transformers\MessageTransformer;
	use frontend\src\transform\transformers\SourceMessageTransformer;
	use frontend\src\transform\transformers\UserTransformer;
	use yii\base\Action;
	use yii\base\InvalidConfigException;
	use yii\filters\AccessControl;
	use yii\web\ForbiddenHttpException;
	
	/**
	 * Class ViewAction
	 *
	 * @property RestController $controller
	 */
	class ViewAction extends Action
	{
		protected $keys = [
			'user' => [
				User::class,
				UserTransformer::class,
			],
			'language' => [
				Language::class,
				LanguageTransformer::class,
			],
			'message' => [
				Message::class,
				MessageTransformer::class,
			],
			'source-message' => [
				SourceMessage::class,
				SourceMessageTransformer::class,
			],
			'domain' => [
				Domain::class,
				DomainTransformer::class,
			],
			'account' => [
				Account::class,
				AccountTransformer::class,
			],
			'company' => [
				Company::class,
				CompanyTransformer::class,
			],
		];
		
		public function run($key, $id)
		{
			if (!in_array($key, array_keys($this->keys))) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.view.error.unknownKey'),
				]);
			}
			
			$key = $this->keys[$key];
			
			/** @var AbstractTransformer $transformer */
			$transformer = new $key[1];
			try {
				$ruler = new AccessControl([
					'rules' => $transformer->accessRules()
				]);
				$ruler->beforeAction($this);
			}   catch (ForbiddenHttpException $e) {
				return $this->controller->module->runAction('error/index', [
					'code' => 403,
					'message' => t('controller', 'rest._behaviors.noAccess')
				]);
			}
			
			/** @var BaseActiveRecord $record */
			$record = new $key[0];
			try {
				$record = $record::find()
					->where([
						"{$record::tableName()}.{$record::idStr()}" => $id,
					])->joinWith($transformer::$loads)
					->one();
			} catch (InvalidConfigException $e) {
				return $this->controller->response(500, [
					'message' => t('controller', 'rest.view.error.badQuery')
				]);
			}
			
			if (!$record) {
				return $this->controller->response(404, [
					'message' => t('controller', 'rest.view.error.recordNotFound'),
				]);
			}
			
			return $this->controller->response(200, [
				'record' => $transformer->one($record),
				'meta' => [],
			]);
		}
	}