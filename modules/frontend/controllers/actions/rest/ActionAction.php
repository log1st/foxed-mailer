<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 14:07
	 */
	
	namespace frontend\controllers\actions\rest;
	
	use common\components\BaseActiveRecord;
	use common\helpers\ArrayHelper;
	use frontend\controllers\RestController;
	use frontend\models\Account;
	use frontend\models\Company;
	use frontend\models\Domain;
	use frontend\models\Language;
	use frontend\models\Message;
	use frontend\models\SourceMessage;
	use frontend\models\User;
	use frontend\src\action\factories\ActionFactory;
	use frontend\src\action\repositories\RequestRepository;
	use Yii;
	use yii\base\Action;
	
	/**
	 * Class ActionAction
	 *
	 * @property RestController $controller
	 */
	class ActionAction extends Action
	{
		protected $keys = [
			'message' => Message::class,
			'language' => Language::class,
			'sourceMessage' => SourceMessage::class,
			'user' => User::class,
			'company' => Company::class,
			'domain' => Domain::class,
			'account' => Account::class
		];
		
		/**
		 * @param $name
		 * @return \yii\web\Response
		 * @throws \yii\base\InvalidConfigException
		 */
		public function run($name)
		{
			$r = Yii::$app->request;
			
			if (!class_exists($name)) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.action.error.unknownAction')
				]);
			}
			
			$names = explode('\\', $name);
			$key = $names[ count($names) - 2 ];
			
			$model = null;
			if (in_array($key, array_keys($this->keys))) {
				$model = $this->keys[ $key ];
			}
			
			if (!class_exists($model)) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.action.error.noModelClass')
				]);
			}
			
			/** @var BaseActiveRecord $model */
			$model = new $model;
			
			if (!($model instanceof BaseActiveRecord)) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.action.error.modelClassIsNotValid')
				]);
			}
			
			$record = $r->post('record');
			
			if ($record) {
				$model = $model::findOne($record);
			}
			
			/** @var ActionFactory $action */
			$action = new $name($model, new RequestRepository($r));
			
			if (!($action instanceof ActionFactory)) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.action.error.unknownAction')
				]);
			}
			
			if (!$action->validate()) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.action.error.validating'),
					'errors' => $action->getRecord()->errors
				]);
			}
			
			try {
				$data = $action->handle();
			} catch (\Exception $e) {
				return $this->controller->response(500, [
					'message' => $action->fail(),
					'reason' => $e->getMessage()
				]);
			}
			
			return $this->controller->response(200, ArrayHelper::merge([
				'message' => $action->succeed()
			], (array)(isset($data) ? [
				'commands' => $data
			] : [])));
		}
	}