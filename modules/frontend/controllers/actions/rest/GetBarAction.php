<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 14:07
	 */
	
	namespace frontend\controllers\actions\rest;
	
	use frontend\controllers\RestController;
	use frontend\src\bar\abstractions\AbstractBar;
	use frontend\src\bar\concretes\AccountViewBar;
	use frontend\src\bar\concretes\CompanyViewBar;
	use frontend\src\bar\concretes\DocumentationBar;
	use frontend\src\bar\concretes\DomainViewBar;
	use frontend\src\bar\concretes\IndexBar;
	use frontend\src\bar\concretes\SignInBar;
	use frontend\src\bar\concretes\SourceMessageViewBar;
	use frontend\src\bar\concretes\TableBar;
	use frontend\src\bar\concretes\UserViewBar;
	use frontend\src\bar\concretes\LanguageViewBar;
	use frontend\src\bar\concretes\MessageViewBar;
	use yii\base\Action;
	
	/**
	 * Class GetBarAction
	 *
	 * @property RestController $controller
	 */
	class GetBarAction extends Action
	{
		protected $routes = [
			'index' => IndexBar::class,
			'table' => TableBar::class,
			'documentation' => DocumentationBar::class,
			'sign-in' => SignInBar::class,
			'user' => UserViewBar::class,
			'language' => LanguageViewBar::class,
			'message' => MessageViewBar::class,
			'source-message' => SourceMessageViewBar::class,
			'domain' => DomainViewBar::class,
			'account' => AccountViewBar::class,
			'company' => CompanyViewBar::class
		];
		
		public function run($route)
		{
			if(!in_array($route, array_keys($this->routes))) {
				return $this->controller->response(400, [
					'message' => t('controller', 'rest.get-bar.error.unknownRoute')
				]);
			}
			
			$bar = $this->routes[$route];
			/** @var AbstractBar $bar */
			$bar = new $bar($route, \Yii::$app->request->get('params'));
			$data = $bar->getData();
			
			return $this->controller->response(200, $data);
		}
	}