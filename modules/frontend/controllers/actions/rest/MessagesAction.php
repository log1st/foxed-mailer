<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 14:07
	 */
	
	namespace frontend\controllers\actions\rest;
	
	
	use common\components\db\Query;
	use common\helpers\ArrayHelper;
	use frontend\controllers\RestController;
	use yii\base\Action;
	use yii\db\Expression;
	
	/**
	 * Class MessagesAction
	 * @package frontend\controllers\actions\spa
	 *
	 * @property RestController $controller
	 */
	class MessagesAction extends Action
	{
		public function run($key) {
			$query = (new Query)
				->select([
					'message' => new Expression("CONCAT(category, '::', message)"),
					'translation' => new Expression("m.translation"),
				])
				->from([
					's' => 'source_messages',
					'm' => 'messages',
					'l' => 'languages',
				])
				->where([
					'm.source' => new Expression('s.source_id'),
					'm.language' => new Expression('l.language_id'),
					'l.locale' => \Yii::$app->language,
				])
				->andWhere([
					'=', new Expression('LOWER(category)'), \Yii::$app->id . '::js::' . $key,
				]);
			
			$messages = [];
			$pattern = '/(' . \Yii::$app->id . '::js::' . $key . '::)/';
			foreach ($query->lists('message', 'translation') as $key => $value) {
				$messages[preg_replace($pattern, '', $key)] = $value;
			}
			
			$locale = explode('-', \Yii::$app->language)[0];
			
			
			return $this->controller->response(200, [
				'messages' => [
					$locale => ArrayHelper::dotExplode($messages)
				],
			]);
		}
	}