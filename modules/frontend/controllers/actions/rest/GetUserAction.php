<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 14:07
	 */
	
	namespace frontend\controllers\actions\rest;
	
	
	use common\models\User;
	use frontend\controllers\RestController;
	use frontend\src\transform\transformers\UserTransformer;
	use yii\base\Action;
	use yii\base\InvalidConfigException;
	
	/**
	 * Class GetUserAction
	 *
	 * @property RestController $controller
	 */
	class GetUserAction extends Action
	{
		public function run()
		{
			/** @var User $user */
			try {
				$user = User::find()->where([
					"users." . User::idStr() => \Yii::$app->user->id,
				])->joinWith([
					'_assignments._item',
				])->one();
			} catch (InvalidConfigException $e) {
			}
			
			if (!isset($user) || !$user) {
				return $this->controller->response(404, [
					'message' => t('controller', 'rest.getUser.notFound'),
				]);
			}
			
			return $this->controller->response(200, [
				'user' => (new UserTransformer)->one($user)
			]);
		}
	}