<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 12:00
	 */
	
	namespace frontend\controllers;
	
	
	use frontend\components\BaseFrontendController;
	use frontend\controllers\actions\error\IndexAction;
	
	class ErrorController extends BaseFrontendController
	{
		public function actions() {
			return [
				'index' => [
					'class' => IndexAction::className()
				]
			];
		}
	}