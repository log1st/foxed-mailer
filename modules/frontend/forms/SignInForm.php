<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 3:46
	 */
	
	namespace frontend\forms;
	
	
	use frontend\models\User;
	use yii\base\InvalidConfigException;
	use yii\base\InvalidParamException;
	use yii\base\Model;
	
	class SignInForm extends Model
	{
		public $login;
		public $password;
		public $remember;
		
		/** @var User */
		protected $user;
		
		public function scenarios()
		{
			return [
				'sign-in' => ['login', 'password', 'remember'],
			];
		}
		
		public function rules()
		{
			return [
				[['login'], 'required', 'message' => t('form', 'SignIn.rules.login.required')],
				[['login'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'email', 'message' => t('form', 'SignIn.rules.login.exist')],
				
				[['password'], 'required', 'message' => t('form', 'SignIn.rules.password.required')],
				[['password'], 'auth', 'message' => t('form', 'SignIn.rules.password.auth'), 'when' => function ($record) {
					/** @var self $record */
					return !$record->hasErrors('login');
				}],
				
				[['remember'], 'required', 'message' => t('form', 'SignIn.rules.remember.required')],
				[['remember'], 'in', 'range' => [0, 1], 'message' => t('form', 'SignIn.rules.remember.in')],
			];
		}
		
		public function auth($attribute, $params)
		{
			$message = $params['message'] ?? t('form', 'SignIn.rules.' . $attribute . '.auth');
			try {
				$user = User::find()->where([
					'email' => $this->login,
				])->one();
			} catch (InvalidConfigException $e) {
				$this->addError($attribute, $message);
				return;
			}
			
			
			try {
				$valid = \Yii::$app->security->validatePassword($this->{$attribute}, $user->password);
			} catch (InvalidParamException $e) {
				$this->addError($attribute, $message);
				return;
			}
			
			if (!isset($valid) || !$valid) {
				$this->addError($attribute, $message);
				return;
			} else {
				$this->user = $user;
			}
		}
		
		/**
		 * @return User
		 */
		public function getUser()
		{
			return $this->user;
		}
	}