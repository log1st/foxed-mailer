<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 12:01
	 */
	
	namespace frontend\components;
	
	
	use common\helpers\ArrayHelper;
	use Yii;
	use yii\debug\Module;
	use yii\debug\Panel;
	use yii\web\Response;
	
	class BaseJsonFrontendController extends BaseFrontendController
	{
		
		/**
		 * @param $action
		 * @return bool
		 * @throws \yii\web\BadRequestHttpException
		 */
		public function beforeAction($action)
		{
			if (\Yii::$app->request->headers->get('Ajax') !== '1') {
				echo $this->renderPartial('//layouts/spa');
				return false;
			} else {
				\Yii::$app->response->format = Response::FORMAT_JSON;
			}
			return parent::beforeAction($action);
		}
	}