<?php
	$params = array_merge(
		require(__DIR__ . '/../../../common/config/params.php'),
		require(__DIR__ . '/../../../common/config/params-local.php'),
		require(__DIR__ . '/params.php'),
		require(__DIR__ . '/params-local.php')
	);
	
	return [
		'id' => 'static',
		'basePath' => dirname(__DIR__),
		'controllerNamespace' => 'static\controllers',
		'params' => $params,
		'components' => [
			'urlManager' => [
				'rules' => include('rules.php'),
			],
		]
	];