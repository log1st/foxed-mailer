<?php
	
	
	use console\components\Migration;
	
	class m170824_113434_sessions extends Migration
	{
		public function safeUp()
		{
			$this->createTable('sessions', [
				'session_id' => $this->primaryKey()->comment('Session ID'),
				'user' => $this->integer()->notNull()->comment('User ID')->null(),
				'hash' => $this->char(40)->notNull()->comment('SESSION ID hash'),
				'expire' => $this->integer()->comment('Expire time'),
				'data' => $this->binary()->comment('Data'),
			]);
			$this->createTimestamps('sessions');
			
			$this->addForeignKey(
				'fk-sessions-users',
				'sessions',
				'user',
				'users',
				'user_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
		}
		
		public function safeDown()
		{
			$this->dropForeignKey('fk-sessions-users', 'sessions');
			
			$this->dropTable('sessions');
			
			return true;
		}
	}
