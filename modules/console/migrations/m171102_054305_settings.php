<?php
	
	
	use console\components\Migration;
	
	class m171102_054305_settings extends Migration
	{
		public function safeUp()
		{
			$this->createTable('settings', [
				'setting_id' => $this->primaryKey()->comment('Setting ID'),
				'name' => $this->string(64)->comment('Name'),
				'value' => $this->text()->null()->comment('Value'),
				'type' => $this->integer()->defaultValue(1)->comment('Type'),
				'parent' => $this->integer()->null()->comment('Parent'),
			]);
			
			$this->createTimestamps('settings');
			
			
			$this->addForeignKey(
				'fk-settings-settings',
				'settings',
				'parent',
				'settings',
				'setting_id',
				static::FK_CASCADE,
				static::FK_CASCADE
			);
		}
		
		public function safeDown()
		{
			
			$this->dropForeignKey('fk-settings-settings', 'settings');
			
			$this->dropTable('settings');
			
			return true;
		}
	}
