<?php
	
	use console\components\Migration;
	
	class m171102_052305_geo extends Migration
	{
		public function safeUp()
		{
			$this->createTable('countries', [
				'country_id' => $this->primaryKey(11)->notNull()->comment('Country ID'),
				'idd' => $this->integer(11)->notNull()->comment('Phone code'),
				'tnp' => $this->string(255)->notNull()->comment('Phone format'),
				'currency' => $this->string(255)->notNull()->comment('Currency name'),
				'currency_short' => $this->string(255)->notNull()->comment('Currency short name'),
				'currency_symbol' => $this->string(255)->notNull()->comment('Currency symbol'),
				'code' => $this->string(4)->notNull()->comment('ip2nation code'),
				'iso_code_2' => $this->string(2)->comment('2 symbols ISO code'),
				'iso_code_3' => $this->string(3)->comment('3 symbols ISO code'),
				'iso_country_name' => $this->string(255)->notNull()->comment('Full ISO country name'),
				'name' => $this->string(255)->notNull()->comment('Name'),
				'latitude' => $this->float()->notNull()->defaultValue(0)->comment('Geo latitude'),
				'longitude' => $this->float()->notNull()->defaultValue(0)->comment('Geo longitude'),
				'status' => $this->integer(1)->notNull()->defaultValue(0)->comment('Status'),
			]);
			
			$this->createTimestamps('countries');
			
			$this->createTable('ip2country', [
				'ip' => $this->integer(11)->unsigned()->comment('IP-address encoded via INET_ATON'),
				'code' => $this->string(4)->notNull()->defaultValue(0)->comment('Country `code`'),
			]);
			
			/**
			 * Thanksgiving to ip2nation team!
			 */
			$this->execute(file_get_contents(__DIR__ . '/ip2nation.sql'));
			
			$this->createTable('cities', [
				'city_id' => $this->primaryKey()->notNull()->comment('City ID'),
				'country' => $this->integer(11)->notNull()->comment('Country `country_id`'),
				'code' => $this->string(4)->notNull()->comment('Unique code with country'),
				'name' => $this->string(255)->notNull()->comment('Name'),
				'longitude' => $this->float()->notNull()->defaultValue(0)->comment('Geo longitude'),
				'latitude' => $this->float()->notNull()->defaultValue(0)->comment('Geo latitude'),
			]);
			
			$this->createTimestamps('cities');
			
			$this->addForeignKey(
				'fk-cities-countries',
				'cities',
				'country',
				'countries',
				'country_id',
				self::FK_NO_ACTION,
				self::FK_CASCADE
			);
			
			$this->addColumn('languages', 'country', $this->integer()->comment('Country'));
			
			$this->addForeignKey(
				'fk-languages-countries',
				'languages',
				'country',
				'countries',
				'country_id',
				self::FK_NO_ACTION,
				self::FK_CASCADE
			);
		}
		
		public function safeDown()
		{
			$this->dropForeignKey('fk-languages-countries', 'languages');
			$this->dropColumn('languages', 'country');
			
			$this->dropForeignKey('fk-cities-countries', 'cities');
			$this->dropTable('cities');
			
			$this->dropTable('ip2country');
			$this->dropTable('countries');
			
			return true;
		}
	}
