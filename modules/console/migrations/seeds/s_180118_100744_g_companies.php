<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:44
 *
 * Seeds data from companies with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100744_g_companies extends Seed
{

    public $table = 'companies';

    public function columns()
    {
        return [
            'company_id', 'name', 'user', 'token'
        ];
    }

    public function rows()
    {
        return [
            [ '1','Foxed','1','fuckingToken' ],
            [ '2','365 Global Markets','1','' ],
            [ '3','G8Markets','1','yetAnotherToken' ],
            [ '4','72Option','1','awesomeToken' ],
            [ '5','IvoryOption','2','noToken' ]
        ];
    }
}