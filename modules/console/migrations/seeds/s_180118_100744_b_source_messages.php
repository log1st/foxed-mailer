<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:44
 *
 * Seeds data from source_messages with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100744_b_source_messages extends Seed
{

    public $table = 'source_messages';

    public function columns()
    {
        return [
            'source_id', 'category', 'message'
        ];
    }

    public function rows()
    {
        return [
            [ '1','frontend::js::frontend','partials.Header.brand' ],
            [ '2','frontend::js::frontend','navigation.documentation' ],
            [ '3','frontend::js::frontend','page.doc.title.developer' ],
            [ '4','frontend::js::frontend','page.doc.title.client' ],
            [ '6','frontend::controller','rest.getUser.notFound' ],
            [ '7','frontend::js::frontend','navigation.sign-in' ],
            [ '8','frontend::js::frontend','page.signIn.form.login' ],
            [ '9','frontend::js::frontend','page.signIn.form.password' ],
            [ '10','frontend::js::frontend','page.signIn.form.remember' ],
            [ '11','frontend::js::frontend','page.signIn.form.submit' ],
            [ '12','frontend::form','SignIn.rules.login.required' ],
            [ '13','frontend::form','SignIn.rules.login.exist' ],
            [ '14','frontend::form','SignIn.rules.password.required' ],
            [ '15','frontend::form','SignIn.rules.password.auth' ],
            [ '16','frontend::form','SignIn.rules.remember.required' ],
            [ '17','frontend::form','SignIn.rules.remember.in' ],
            [ '18','frontend::controller','auth.SignIn.error.validating' ],
            [ '20','frontend::controller','auth.SignIn.success' ],
            [ '21','frontend::js::frontend','navigation.sign-out' ],
            [ '22','frontend::controller','auth.SignOut.success' ],
            [ '23','frontend::controller','auth.SignOut.error.already' ],
            [ '24','frontend::js::frontend','page.doc.pageTitle' ],
            [ '25','frontend::js::frontend','page.index.pageTitle' ],
            [ '26','frontend::js::frontend','page.sign-in.pageTitle' ],
            [ '27','frontend::js::frontend','partials.Sidebar.navigation.companies' ],
            [ '28','frontend::js::frontend','partials.Sidebar.navigation.accounts' ],
            [ '29','frontend::js::frontend','partials.Sidebar.navigation.domains' ],
            [ '30','frontend::js::frontend','page.List.title.accounts' ],
            [ '31','frontend::js::frontend','page.List.title.companies' ],
            [ '32','frontend::js::frontend','page.List.title.domains' ],
            [ '35','frontend::controller','rest.table.error.unknownModel' ],
            [ '36','frontend::js::frontend','model.Company.company_id' ],
            [ '37','frontend::js::frontend','model.Company.name' ],
            [ '38','frontend::js::frontend','model.Company.token' ],
            [ '39','frontend::js::frontend','model.Company.updated_at' ],
            [ '40','frontend::js::frontend','model.Company.created_at' ],
            [ '41','frontend::js::frontend','partials.Fields.empty' ],
            [ '44','frontend::js::frontend','model.Account.name' ],
            [ '45','frontend::js::frontend','model.Account.account_id' ],
            [ '46','frontend::js::frontend','model.Account.company' ],
            [ '47','frontend::js::frontend','model.Account.port' ],
            [ '48','frontend::js::frontend','model.Account.host' ],
            [ '49','frontend::js::frontend','model.Account.encryption' ],
            [ '50','frontend::js::frontend','model.Account.created_at' ],
            [ '51','frontend::js::frontend','model.Account.updated_at' ],
            [ '52','frontend::js::frontend','model.Domain.domain_id' ],
            [ '53','frontend::js::frontend','model.Domain.host' ],
            [ '54','frontend::js::frontend','model.Domain.company' ],
            [ '55','frontend::js::frontend','model.Domain.created_at' ],
            [ '56','frontend::js::frontend','model.Domain.updated_at' ],
            [ '57','frontend::js::frontend','partials.Footer.brand' ],
            [ '58','frontend::js::frontend','partials.Footer.description' ],
            [ '59','frontend::js::frontend','partials.Sidebar.navigation.source-messages' ],
            [ '60','frontend::js::frontend','page.List.title.source-messages' ],
            [ '61','frontend::js::frontend','model.SourceMessage.source_id' ],
            [ '62','frontend::js::frontend','model.SourceMessage.category' ],
            [ '63','frontend::js::frontend','model.SourceMessage.updated_at' ],
            [ '64','frontend::js::frontend','model.SourceMessage.created_at' ],
            [ '65','frontend::js::frontend','model.SourceMessage.message' ],
            [ '66','frontend::js::frontend','partials.Sidebar.navigation.messages' ],
            [ '67','frontend::js::frontend','partials.Sidebar.navigation.languages' ],
            [ '68','frontend::js::frontend','page.List.title.messages' ],
            [ '69','frontend::js::frontend','page.List.title.languages' ],
            [ '70','frontend::js::frontend','model.Message.message_id' ],
            [ '71','frontend::js::frontend','model.Message.source' ],
            [ '72','frontend::js::frontend','model.Message.created_at' ],
            [ '73','frontend::js::frontend','model.Message.language' ],
            [ '74','frontend::js::frontend','model.Message.translation' ],
            [ '75','frontend::js::frontend','model.Language.language_id' ],
            [ '76','frontend::js::frontend','model.Language.name' ],
            [ '77','frontend::js::frontend','model.Language.country' ],
            [ '78','frontend::js::frontend','model.Language.status' ],
            [ '79','frontend::js::frontend','model.Language.created_at' ],
            [ '80','frontend::js::frontend','model.Language.locale' ],
            [ '81','frontend::js::frontend','model.Message.updated_at' ],
            [ '82','frontend::js::frontend','model.Language.updated_at' ],
            [ '83','frontend::js::frontend','partials.Fields.boolean.true' ],
            [ '84','frontend::js::frontend','partials.Fields.boolean.false' ],
            [ '87','frontend::js::frontend','partials.Fields.boolean.Language.enabled' ],
            [ '88','frontend::js::frontend','partials.Fields.boolean.Language.disabled' ],
            [ '89','frontend::js::frontend','partials.Sidebar.navigation.users' ],
            [ '90','frontend::js::frontend','page.List.title.users' ],
            [ '91','frontend::js::frontend','model.User.user_id' ],
            [ '92','frontend::js::frontend','model.User.updated_at' ],
            [ '93','frontend::js::frontend','model.User.email' ],
            [ '94','frontend::js::frontend','model.User.created_at' ],
            [ '95','frontend::controller','rest.get-bar.error.unknownRoute' ],
            [ '117','frontend::js::frontend','page.List.noRecords' ],
            [ '118','frontend::js::frontend','partials.Bar.title.index' ],
            [ '119','frontend::js::frontend','page.List.summary' ],
            [ '128','frontend::js::frontend','partials.Bar.title.table.companies' ],
            [ '135','frontend::js::frontend','partials.Bar.title.table.accounts' ],
            [ '136','frontend::js::frontend','partials.Bar.title.table.domains' ],
            [ '137','frontend::js::frontend','partials.Bar.title.table.source-messages' ],
            [ '138','frontend::js::frontend','partials.Bar.title.table.messages' ],
            [ '139','frontend::js::frontend','partials.Bar.title.table.languages' ],
            [ '140','frontend::js::frontend','partials.Bar.title.table.users' ],
            [ '141','frontend::js::frontend','partials.Bar.title.documentation' ],
            [ '142','frontend::js::frontend','partials.Bar.title.sign-in' ],
            [ '143','frontend::js::frontend','page.List.pagination.first' ],
            [ '144','frontend::js::frontend','page.List.pagination.last' ],
            [ '147','frontend::js::frontend','partial.Bar.control.table-filters.trigger' ],
            [ '148','frontend::js::frontend','partial.Bar.control.table-controls.trigger' ],
            [ '153','frontend::js::frontend','partials.Bar.partials.TableControls.limit' ],
            [ '155','frontend::controller','rest.view.error.recordNotFound' ],
            [ '156','frontend::js::frontend','page.User.title' ],
            [ '157','frontend::js::frontend','pages.User.partials.Profile.title' ],
            [ '158','frontend::js::frontend','pages.User.partials.Profile.basic.title' ],
            [ '159','frontend::js::frontend','pages.User.partials.Profile.cell.caption.attribute.email' ],
            [ '160','frontend::js::frontend','pages.User.partials.Profile.cell.caption.attribute.created_at' ],
            [ '161','frontend::js::frontend','pages.User.partials.Profile.roles.title' ],
            [ '162','frontend::js::frontend','access.Role.manager' ],
            [ '163','frontend::js::frontend','access.Role.admin' ],
            [ '164','frontend::js::frontend','access.Role.developer' ],
            [ '165','frontend::js::frontend','access.Permission.companies.index' ],
            [ '166','frontend::js::frontend','access.Permission.documentation' ],
            [ '167','frontend::js::frontend','access.Permission.accounts.index' ],
            [ '168','frontend::js::frontend','access.Permission.domains.index' ],
            [ '169','frontend::js::frontend','access.Permission.messages.index' ],
            [ '170','frontend::js::frontend','access.Permission.source-messages.index' ],
            [ '171','frontend::js::frontend','access.Permission.languages.index' ],
            [ '172','frontend::js::frontend','access.Permission.debug' ],
            [ '173','frontend::js::frontend','pages.User.partials.Profile.permissions.title' ],
            [ '174','frontend::js::frontend','partials.Bar.title.user' ],
            [ '175','frontend::js::frontend','pages.Language.partials.Profile.title' ],
            [ '176','frontend::js::frontend','page.Language.title' ],
            [ '177','frontend::js::frontend','pages.Language.partials.Profile.basic.title' ],
            [ '180','frontend::js::frontend','pages.Language.partials.Profile.cell.caption.attribute.locale' ],
            [ '181','frontend::js::frontend','pages.Language.partials.Profile.cell.caption.attribute.name' ],
            [ '182','frontend::js::frontend','pages.Language.partials.Profile.cell.caption.attribute.country' ],
            [ '183','frontend::js::frontend','pages.Language.partials.Profile.cell.caption.attribute.status' ],
            [ '184','frontend::js::frontend','partials.Bar.title.language' ],
            [ '185','frontend::js::frontend','partials.Bar.title.message' ],
            [ '186','frontend::controller','rest.view.error.unknownKey' ],
            [ '187','frontend::js::frontend','pages.Message.partials.Profile.title' ],
            [ '188','frontend::js::frontend','pages.Message.partials.Profile.basic.title' ],
            [ '189','frontend::js::frontend','page.Message.title' ],
            [ '190','frontend::js::frontend','pages.Message.partials.Profile.cell.caption.attribute.translation' ],
            [ '191','frontend::js::frontend','pages.Message.partials.Profile.cell.caption.attribute.language' ],
            [ '192','frontend::js::frontend','pages.Message.partials.Profile.cell.caption.attribute.source' ],
            [ '193','frontend::js::frontend','page.SourceMessage.title' ],
            [ '200','frontend::js::frontend','pages.SourceMessage.partials.Profile.title' ],
            [ '201','frontend::js::frontend','pages.SourceMessage.partials.Profile.basic.title' ],
            [ '202','frontend::js::frontend','pages.SourceMessage.partials.Profile.cell.caption.attribute.message' ],
            [ '203','frontend::js::frontend','pages.SourceMessage.partials.Profile.cell.caption.attribute.category' ],
            [ '204','frontend::js::frontend','partials.Bar.title.source-message' ],
            [ '205','frontend::js::frontend','partials.Bar.title.domain' ],
            [ '206','frontend::js::frontend','partials.Bar.title.account' ],
            [ '207','frontend::js::frontend','partials.Bar.title.company' ],
            [ '208','frontend::js::frontend','page.Company.title' ],
            [ '209','frontend::js::frontend','pages.Company.partials.Profile.title' ],
            [ '210','frontend::js::frontend','pages.Company.partials.Profile.cell.caption.attribute.user' ],
            [ '211','frontend::js::frontend','pages.Company.partials.Profile.basic.title' ],
            [ '212','frontend::js::frontend','pages.Company.partials.Profile.cell.caption.attribute.name' ],
            [ '213','frontend::js::frontend','pages.Company.partials.Profile.cell.caption.attribute.created_at' ],
            [ '214','frontend::js::frontend','pages.Company.partials.Profile.timestamps.title' ],
            [ '215','frontend::js::frontend','pages.Company.partials.Profile.cell.caption.attribute.updated_at' ],
            [ '216','frontend::js::frontend','pages.Account.partials.Profile.basic.title' ],
            [ '217','frontend::js::frontend','page.Account.title' ],
            [ '218','frontend::js::frontend','pages.Account.partials.Profile.title' ],
            [ '219','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.name' ],
            [ '220','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.company' ],
            [ '221','frontend::js::frontend','pages.Account.partials.Profile.timestamps.title' ],
            [ '222','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.created_at' ],
            [ '223','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.updated_at' ],
            [ '224','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.host' ],
            [ '225','frontend::js::frontend','pages.Account.partials.Profile.SMTP.title' ],
            [ '227','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.encryption' ],
            [ '228','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.login' ],
            [ '229','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.port' ],
            [ '230','frontend::js::frontend','pages.Account.partials.Profile.cell.caption.attribute.password' ],
            [ '231','frontend::js::frontend','pages.Domain.partials.Profile.title' ],
            [ '232','frontend::js::frontend','page.Domain.title' ],
            [ '233','frontend::js::frontend','pages.Domain.partials.Profile.basic.title' ],
            [ '235','frontend::js::frontend','pages.Domain.partials.Profile.cell.caption.attribute.company' ],
            [ '236','frontend::js::frontend','pages.Domain.partials.Profile.timestamps.title' ],
            [ '237','frontend::js::frontend','pages.Domain.partials.Profile.cell.caption.attribute.created_at' ],
            [ '238','frontend::js::frontend','pages.Domain.partials.Profile.cell.caption.attribute.updated_at' ],
            [ '239','frontend::js::frontend','pages.Domain.partials.Profile.cell.caption.attribute.host' ],
            [ '240','frontend::controller','rest._behaviors.noAccess' ],
            [ '241','frontend::js::frontend','page.doc.noPermission' ],
            [ '242','frontend::js::frontend','partials.Exception.title' ],
            [ '243','frontend::js::frontend','partials.Exception.title' ],
            [ '244','frontend::action','message.EditSourceID.title' ],
            [ '245','frontend::action','message.EditSourceID.label.source_id' ],
            [ '246','frontend::action','message.EditSourceID.placeholder.source_id' ],
            [ '247','frontend::js::frontend','partials.Modal.footer.closer' ],
            [ '248','frontend::action','message.EditSource.title' ],
            [ '249','frontend::action','message.EditSource.label.source' ],
            [ '250','frontend::action','message.EditSource.placeholder.source' ],
            [ '251','frontend::action','message.EditLanguage.title' ],
            [ '252','frontend::action','message.EditLanguage.label.language' ],
            [ '253','frontend::action','message.EditLanguage.placeholder.language' ]
        ];
    }
}