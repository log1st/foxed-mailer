<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:44
 *
 * Seeds data from accounts with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100744_i_accounts extends Seed
{

    public $table = 'accounts';

    public function columns()
    {
        return [
            'account_id', 'name', 'company', 'encryption', 'host', 'port', 'login', 'password', 'external_id'
        ];
    }

    public function rows()
    {
        return [
            [ '1','noreply','1','SSL','smtp.foxed.io','25','noreply','AwesomePassword','' ]
        ];
    }
}