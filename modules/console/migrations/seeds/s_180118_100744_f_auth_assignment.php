<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:44
 *
 * Seeds data from auth_assignment with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100744_f_auth_assignment extends Seed
{

    public $table = 'auth_assignment';

    public function columns()
    {
        return [
            'auth_assignment_id', 'item_id', 'user_id'
        ];
    }

    public function rows()
    {
        return [
            [ '1','1','1' ],
            [ '2','12','1' ]
        ];
    }
}