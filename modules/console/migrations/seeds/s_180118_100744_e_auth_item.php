<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:44
 *
 * Seeds data from auth_item with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100744_e_auth_item extends Seed
{

    public $table = 'auth_item';

    public function columns()
    {
        return [
            'auth_item_id', 'name', 'type', 'description', 'rule_name', 'data'
        ];
    }

    public function rows()
    {
        return [
            [ '1','admin','1','Admin','','' ],
            [ '2','client','1','','','' ],
            [ '3','developer','1','','','' ],
            [ '4','manager','1','','','' ],
            [ '5','documentation','2','','','' ],
            [ '6','companies.index','2','','','' ],
            [ '7','accounts.index','2','','','' ],
            [ '8','domains.index','2','','','' ],
            [ '9','source-messages.index','2','','','' ],
            [ '10','messages.index','2','','','' ],
            [ '11','languages.index','2','','','' ],
            [ '12','debug','2','','','' ],
            [ '13','companies.view','2','','','' ],
            [ '14','accounts.view','2','','','' ],
            [ '15','domains.view','2','','','' ],
            [ '16','source-messages.view','2','','','' ],
            [ '17','messages.view','2','','','' ],
            [ '18','languages.view','2','','','' ],
            [ '19','users.index','2','','','' ],
            [ '20','users.view','2','','','' ]
        ];
    }
}