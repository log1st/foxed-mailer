<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:44
 *
 * Seeds data from domains with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100744_h_domains extends Seed
{

    public $table = 'domains';

    public function columns()
    {
        return [
            'domain_id', 'host', 'company', 'external_id'
        ];
    }

    public function rows()
    {
        return [
            [ '1','foxed.io','1','' ],
            [ '2','log1st.net','2','' ],
            [ '3','google.com','1','' ]
        ];
    }
}