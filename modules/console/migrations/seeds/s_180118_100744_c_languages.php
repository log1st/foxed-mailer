<?php
/**
 * Created by log1st via Seeder Generator.
 * Date: 2018-01-18
 * Time: 11:07:44
 *
 * Seeds data from languages with all records
 * Previous seed doesn't exist
 */

namespace console\migrations\seeds;


use common\models\Language;
use console\components\Seed;

class s_180118_100744_c_languages extends Seed
{

    public $table = 'languages';

    public function columns()
    {
        return [
            'language_id', 'locale', 'name', 'status', 'country'
        ];
    }

    public function rows()
    {
        return [
            [ '1','en-US','English (USA)','1','221' ],
            [ '2','ru-RU','Русский','0','181' ]
        ];
    }
}