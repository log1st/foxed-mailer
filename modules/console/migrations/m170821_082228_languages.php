<?php
	
	
	use console\components\Migration;
	
	class m170821_082228_languages extends Migration
	{
		public function safeUp()
		{
			
			$this->createTable('languages', [
				'language_id' => $this
					->primaryKey()
					->comment('Language ID'),
				'locale' => $this
					->string(10)
					->comment('Locale'),
				'name' => $this
					->string()
					->comment('Name'),
			]);
			$this->createTimestamps('languages');
			
			$this->alterColumn('messages', 'language', $this->integer()->notNull()->comment('Language ID'));
			
			$this->addForeignKey(
				'fk-messages-languages',
				'messages',
				'language',
				'languages',
				'language_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
		}
		
		public function safeDown()
		{
			$this->dropForeignKey('fk-messages-languages', 'messages');
			
			$this->alterColumn('messages', 'language', $this->string(16));
			
			$this->dropTable('languages');
			
			return true;
		}
	}
