<?php
	
	
	use console\components\Migration;
	
	class m170814_030634_i18n extends Migration
	{
		/**
		 * Creates all rbac default tables
		 */
		/*
		private function runBaseI18nMigration() {
			$oldApp = \Yii::$app;
			new \yii\console\Application([
					'id' => 'Command runner',
					'basePath' => '@app',
					'components' => $oldApp->components
				]
			);
			Yii::$app->runAction('migrate/up', [
				'migrationPath' => '@yii/i18n/migrations/',
				'interactive' => false
			]);
			Yii::$app = $oldApp;
			$this->dropTable('source_message');
			$this->dropTable('message');
		}
		*/
		
		public function safeUp()
		{
			//$this->runBaseI18nMigration();
			
			$this->createTable('source_messages', [
				'source_id' => $this
					->primaryKey(11)
					->notNull(),
				'category' => $this
					->string(255),
				'message' => $this
					->text(),
			]);
			$this->createTimestamps('source_messages');
			
			$this->createTable('messages', [
				'message_id' => $this
					->primaryKey(11)
					->notNull(),
				'source' => $this
					->integer(11)
					->notNull(),
				'language' => $this
					->string(16),
				'translation' => $this
					->text(),
			]);
			$this->createTimestamps('messages');
			
			$this->addForeignKey(
				'fk-message-source',
				'messages',
				'source',
				'source_messages',
				'source_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
		}
		
		public function safeDown()
		{
			$this->dropForeignKey(
				'fk-message-source',
				'messages'
			);
			
			$this->dropTable('messages');
			
			$this->dropTable('source_messages');
			
			return true;
		}
	}
