<?php
	
	use console\components\Migration;
	
	
	/**
	 * Class m180117_195824_users_parent_column
	 */
	class m180117_195824_users_parent_column extends Migration
	{
		/**
		 * @inheritdoc
		 */
		public function safeUp()
		{
			$this->addColumn('users', 'parent', $this->integer()->null()->comment('Parent ID'));
		}
		
		/**
		 * @inheritdoc
		 */
		public function safeDown()
		{
			$this->dropColumn('users', 'parent');
			
			return true;
		}
	}
