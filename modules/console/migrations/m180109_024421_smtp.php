<?php
	
	use console\components\Migration;
	
	
	/**
	 * Class m180109_024421_smtp
	 */
	class m180109_024421_smtp extends Migration
	{
		/**
		 * @inheritdoc
		 */
		public function safeUp()
		{
			$this->createTable('companies', [
				'company_id' => $this->primaryKey()->comment('Company ID'),
				'name' => $this->string(1020)->comment('Name'),
				'user' => $this->integer()->comment('User'),
				'token' => $this->string(255)->comment('Token')
			]);
			$this->createTimestamps('companies');
			
			$this->addForeignKey(
				'fk-companies-users',
				'companies',
				'user',
				'users',
				'user_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
			
			$this->createTable('accounts', [
				'account_id' => $this->primaryKey()->comment('Account ID'),
				'name' => $this->string(1020)->comment('Name'),
				'company' => $this->integer()->comment('Company'),
				'encryption' => $this->string(16)->comment('Encryption Type'),
				'host' => $this->string(1020)->comment('Host'),
				'port' => $this->integer(6)->comment('Port'),
				'login' => $this->string(1020)->comment('Login'),
				'password' => $this->string(1020)->comment('Password'),
				'external_id' => $this->integer()->null()->comment('External smtp ID')
			]);
			$this->createTimestamps('accounts');
			
			$this->addForeignKey(
				'fk-accounts-companies',
				'accounts',
				'company',
				'companies',
				'company_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
			
			$this->createTable('domains', [
				'domain_id' => $this->primaryKey()->comment('Domain ID'),
				'host' => $this->string(1020)->comment('Host'),
				'company' => $this->integer()->comment('Company'),
				'external_id' => $this->integer()->null()->comment('External smtp ID')
			]);
			$this->createTimestamps('domains');
			
			$this->addForeignKey(
				'fk-domains-companies',
				'domains',
				'company',
				'companies',
				'company_id',
				self::FK_CASCADE,
				self::FK_CASCADE
			);
		}
		
		/**
		 * @inheritdoc
		 */
		public function safeDown()
		{
			$this->dropForeignKey('fk-domains-companies', 'domains');
			$this->dropTable('domains');
			
			$this->dropForeignKey('fk-accounts-companies', 'accounts');
			$this->dropTable('accounts');
			
			$this->dropForeignKey('fk-companies-users', 'companies');
			$this->dropTable('companies');
			
			return true;
		}
	}
