<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 22.10.2017
	 * Time: 9:38
	 */
	
	namespace console\components;
	
	use Yii;
	use yii\helpers\ArrayHelper;
	
	abstract class Seed extends Migration
	{
		const EVENT_BEFORE_SEEDING = 'beforeSeeding';
		const EVENT_AFTER_SEEDING = 'afterSeeding';
		
		/** @var string table name */
		public $table = null;
		/** @var array array of columns names */
		public $columns = [];
		/** @var array array of array of values */
		public $rows = [];
		/** @var bool|array tables to truncate w/o $this->table */
		public $clear = false;
		
		public abstract function columns();
		
		public abstract function rows();
		
		public function beforeSeeding()
		{
		}
		
		public function afterSeeding()
		{
		}
		
		public function __construct(array $config = [])
		{
			$config = ArrayHelper::merge($config, [
				'columns' => array_merge($this->columns(), $del = [
					'created_at',
					'updated_at',
				]),
				'rows' => array_map(function ($item) use ($del) {
					foreach ($del as $d) {
						if (isset($item[$d])) {
							unset($item[$d]);
						}
					}
					return array_merge((array)$item, [
						time(), time(),
					]);
				}, $this->rows()),
			]);
			$this->on(static::EVENT_BEFORE_SEEDING, function () {
				$this->beforeSeeding();
			});
			$this->on(static::EVENT_AFTER_SEEDING, function () {
				$this->afterSeeding();
			});
			parent::__construct($config);
		}
		
		public function run()
		{
			$this->trigger(static::EVENT_BEFORE_SEEDING);
			/*
			$fields = [];
			foreach($this->rows as $key => $row) {
				foreach($row as $subKey => $field) {
					$fields[$key][$this->columns[$subKey]] = $field;
				}
			}
			*/
			
			if (!$this->clear === false) {
				Yii::$app->db->createCommand('set foreign_key_checks=0')->execute();
				$clear = array_merge((array)($this->clear === true ? [] : $this->clear), (array)$this->table);
				foreach ($clear as $item) {
					$this->truncateTable($item);
				}
				Yii::$app->db->createCommand('set foreign_key_checks=1')->execute();
			}
			
			$this->batchInsert($this->table, $this->columns, $this->rows);
			$command = $this->insert('seeds', [
				'class' => static::className(),
				'apply_time' => time(),
				'table' => $this->table,
			]);
			if ($command) {
				if (Yii::$app->id === 'console') {
					/** @var BaseConsoleController $ctrl */
					$ctrl = Yii::$app->controller;
					$ctrl->stdout('Seeder ' . $this::className() . ' has been migrated.');
				}
			}
			$this->trigger(static::EVENT_AFTER_SEEDING);
		}
	}