<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 22.10.2017
 * Time: 10:28
 */

namespace console\controllers;


use common\components\db\Query;
use console\components\BaseConsoleController;
use console\components\Seed;
use Yii;

class SeedsController extends BaseConsoleController
{
    public $tables = null;

    public function options($actionID) {
        return [
            'dump' => [
                'tables'
            ]
        ][$actionID];
    }

    public function optionAliases()
    {
        return [
            't' => 'tables'
        ];
    }

    public function actionDump() {
        $tables = array_filter(explode(',', $this->tables));
        if(!count($tables)) {
            echo 'No tables';
            return false;
        }
        $i = 'a';
        foreach($tables as $id => $table) {
            $table = trim($table);
            $lastDump = (new Query())
                ->select(['seed_id', 'apply_time'])
                ->from('seeds')
                ->where(['table' => $table])
                ->orderBy('apply_time desc')
                ->one();
            $query = (new Query())
                ->select('*')
                ->from($table);
            if($lastDump) {
                $query->where(['>', 'created_at', $lastDump['apply_time']]);
            }
            $records = $query->all();
            $del = [ 'created_at', 'updated_at' ];
            foreach($records as &$record) {
                $record = array_diff_key($record, array_flip($del));
            }
            if(count($records)) {
                $className = 's_' . gmdate('ymd_His', time()) . '_'  . ($i) . '_' . mb_strtolower($table);
                $columns = array_keys($records[0]);
                $text = $this->renderPartial('dump', [
                    'className' => $className,
                    'tableName' => $table,
                    'rows' => $records,
                    'columns' => $columns,
                    'date' => date('Y-m-d'),
                    'time' => date('H:i:s'),
                    'applyTime' => ($lastDump ?? false) ? date('Y-m-d H:i:s', $lastDump['apply_time']) : false,
                    'previousSeed' => $lastDump['seed_id'] ?? false
                ]);
                try {
                    $put = file_put_contents(Yii::getAlias('@seeds') . '/' . $className . '.php', $text);
                }   catch (\Exception $e) {
                    $put = false;
                }
                if($put) {
                    Yii::$app->db->createCommand()->insert('seeds', [
                        'class' => 'console\migrations\seeds\\' . $className,
                        'apply_time' => time(),
                        'table' => $table
                    ])->execute();
                    echo 'Seeder ' . $className . ' created.',PHP_EOL;
                }   else {
                    throw new \Exception('Unable to create seeder ' . $className);
                }
            }
            $i = ++$i;
        }
    }

    public function actionSeed() {
        $seeds = array_map(function($item) {
            return basename(str_replace('\\', '/', $item));
        }, (new Query())
            ->select(['class','id' => 'seed_id'])
            ->from('seeds')
            ->lists('id', 'class'));

        $files = array_map(function($item) {
            return explode('.', $item)[0];
        }, array_diff(scandir(Yii::getAlias('@seeds')), array('.', '..')));

        $classes = array_diff($files, $seeds);

        foreach($classes as $class) {
            $class = '\console\migrations\seeds\\' . $class;
            /** @var Seed $seed */
            $seed = new $class;

            $seed->run();
        }

    }
}