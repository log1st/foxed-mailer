<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.08.2017
 * Time: 4:37
 */

use yii\helpers\Html;


/**
 * Implements ucfirst with mb_string
 * @param $str string
 * @return string
 */
function mb_ucfirst($str) {
    $str = mb_strtolower($str);
    $str[0] = mb_strtoupper($str[0]);

    return $str;
}

/**
 * Dumps ActiveQuery rawSql
 * @param \yii\db\ActiveQuery|\yii\db\Query|\yii\db\Command $query
 * @param bool $die
 */
function dq($query, bool $die = true) {
    if($query instanceof \yii\db\ActiveQuery || $query instanceof \yii\db\Query) {
        $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
    }   else {
        $sql = $query->rawSql;
    }

    echo $sql;
    if($die)
        die;
}

/**
 * Pretty pre
 *
 * @param $response
 * @param bool $die
 */
function pre($response, $die = true) {
    Yii::$app->response->format = 'raw';
    ob_start();
    var_dump($response);
    echo Html::tag('pre', ob_get_clean());
    if($die) {
        Yii::$app->end(200);
    }
}

/**
 * Shortcut to Yii::t
 * @inheritdoc BaseYii::t
 */
function t($category, $message, $params = [], $language = null) {
    return Yii::t($category, $message, $params, $language);
}