<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 5:02
	 */
	
	namespace common\models\auth;
	
	
	use common\components\BaseActiveRecord;
	use common\models\User;
	
	/**
	 * Class Item
	 * @package common\models\Auth
	 *
	 * @property integer $auth_assignment_id
	 * @property integer $item_id
	 * @property integer $user_id
	 * @property integer $created_at
	 *
	 * @property User $_user
	 * @property Item $_item
	 */
	class Assignment extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'auth-assignments';
		}
		
		public static function tableName()
		{
			return 'auth_assignment';
		}
		
		public static function idStr()
		{
			return 'auth_assignment_id';
		}
		
		public function get_user()
		{
			return $this->hasOne(User::className(), [
				User::idStr() => 'user_id',
			]);
		}
		
		public function get_item()
		{
			return $this->hasOne(Item::className(), [
				Item::idStr() => 'item_id',
			]);
		}
	}