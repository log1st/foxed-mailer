<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 20.08.2017
	 * Time: 18:52
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	
	/**
	 * Class Message
	 * @package common\models
	 *
	 * @property integer $message_id
	 * @property integer $source
	 * @property integer $language
	 * @property string $translation
	 *
	 * @property SourceMessage $_source
	 * @property Language $_language
	 */
	class Message extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'messages';
		}
		
		public static function tableName()
		{
			return 'messages';
		}
		
		public static function sortAttribute()
		{
			return 'translation';
		}
		
		public static function idStr()
		{
			return 'message_id';
		}
		
		public function get_source()
		{
			return $this->hasOne(SourceMessage::className(), [
				SourceMessage::idStr() => 'source',
			]);
		}
		
		public function get_language()
		{
			return $this->hasOne(Language::className(), [
				Language::idStr() => 'language',
			]);
		}
	}