<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:19
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	use common\models\auth\Assignment;
	use yii\base\NotSupportedException;
	use yii\web\IdentityInterface;
	
	
	/**
	 * Class Company
	 * @package common\models
	 *
	 * @property integer $company_id
	 * @property string $name
	 * @property integer $user
	 * @property string $token
	 *
	 * @property User $_user
	 * @property Account[] $_accounts
	 * @property Domain[] $_domains
	 */
	class Company extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'companies';
		}
		
		public static function tableName()
		{
			return 'companies';
		}
		
		public static function sortAttribute()
		{
			return 'name';
		}
		
		public static function idStr()
		{
			return 'company_id';
		}
		
		public function __toString()
		{
			return (string)$this->name;
		}
		
		public function get_user()
		{
			return $this->hasOne(User::className(), [
				User::idStr() => 'user',
			]);
		}
		
		public function get_accounts()
		{
			return $this->hasMany(Account::className(), [
				'company' => self::idStr(),
			]);
		}
		
		public function get_domains()
		{
			return $this->hasMany(Domain::className(), [
				'company' => self::idStr(),
			]);
		}
		
		/**
		 * @param $insert
		 * @return bool
		 * @throws \yii\base\Exception
		 */
		public function beforeSave($insert)
		{
			if(empty($this->token)) {
				$this->token = \Yii::$app->security->generatePasswordHash(uniqid('company_' . $this->id));
			}
			return parent::beforeSave($insert);
		}
	}