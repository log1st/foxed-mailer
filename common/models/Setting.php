<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 27.08.2017
	 * Time: 3:54
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	
	/**
	 * Class Setting
	 * @package common\models
	 *
	 * @property integer $setting_id
	 * @property string $name
	 * @property string|array $value
	 * @property integer $type
	 * @property integer $parent
	 *
	 * @property Setting $_parent
	 * @property Setting[] $_children
	 */
	class Setting extends BaseActiveRecord
	{
		const TYPE_STRING = 1;
		const TYPE_INTEGER = 2;
		const TYPE_FLOAT = 4;
		const TYPE_ARRAY = 8;
		const TYPE_BOOLEAN = 16;
		
		/** @var Setting[] */
		public $children = [];
		
		public static function cnc()
		{
			return 'settings';
		}
		
		public static function tableName()
		{
			return 'settings';
		}
		
		public static function idStr()
		{
			return 'setting_id';
		}
		
		public function __toString()
		{
			return $this->name;
		}
		
		public function get_parent()
		{
			return $this->hasOne(Setting::className(), [
				Setting::idStr() => 'parent',
			])->from(['settingsParent' => Setting::tableName()]);
		}
		
		public function get_children()
		{
			return $this->hasOne(Setting::className(), [
				'parent' => Setting::idStr(),
			]);
		}
		
		private function parseValueByType()
		{
			if ($this->type === self::TYPE_ARRAY) {
				$this->value = explode(',', $this->value);
			}
		}
		
		public function setScenario($value)
		{
			parent::setScenario($value);
			
			if (in_array($value, ['insert', 'update'])) {
				if ($this->type === self::TYPE_ARRAY && is_array($this->value)) {
					$this->value = implode(',', $this->value);
				}
			} else {
				if ($this->type === self::TYPE_ARRAY && !is_array($this->value)) {
					$this->value = explode(', ', $this->value);
				}
			}
		}
		
		public function afterFind()
		{
			parent::afterFind();
			
			$this->parseValueByType();
		}
		
		/**
		 * @var Setting[]
		 */
		private static $tree = null;
		/** @var array */
		private static $treeArray = null;
		
		/**
		 * @param self[] $records
		 * @param null $id
		 * @return self[]
		 */
		private static function getChildren(&$records, $id = null)
		{
			$children = [];
			
			foreach ($records as $record) {
				if ($record->parent === $id) {
					$record->children = (array)static::getChildren($records, $record->id);
					$children[] = $record;
					unset($records[$record->id]);
				}
			}
			
			return $children;
		}
		
		/**
		 * @param Setting[] $records
		 * @return array
		 */
		private static function childToArray($records)
		{
			$data = [];
			
			foreach ($records as $record) {
				if (!is_null($record->value)) {
					$data[$record->name]['value'] = $record->value;
				}
				$children = static::childToArray($record->children);
				if (count($children)) {
					$data[$record->name]['children'] = $children;
				}
			}
			
			return $data;
		}
		
		public static function tree($asArray = false)
		{
			if (is_null(static::$tree)) {
				$records = self::find()->all();
				$assoc = [];
				foreach ($records as $item) {
					$assoc[$item->id] = $item;
				}
				static::$tree = static::getChildren($assoc);
				static::$treeArray = static::childToArray(static::$tree);
			}
			
			return $asArray ? static::$treeArray : static::$tree;
		}
	}