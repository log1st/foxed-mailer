<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:19
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	use common\models\auth\Assignment;
	use yii\base\NotSupportedException;
	use yii\web\IdentityInterface;
	
	
	/**
	 * Class Account
	 * @package common\models
	 *
	 * @property integer $account_id
	 * @property string $name
	 * @property integer $company
	 * @property string $encryption
	 * @property string $host
	 * @property integer $port
	 * @property string $login
	 * @property string $password
	 * @property integer $external_id
	 *
	 * @property Company $_company
	 */
	class Account extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'accounts';
		}
		
		public static function tableName()
		{
			return 'accounts';
		}
		
		public static function sortAttribute()
		{
			return 'name';
		}
		
		public static function idStr()
		{
			return 'account_id';
		}
		
		public function __toString()
		{
			return (string)$this->name;
		}
		
		public function get_company()
		{
			return $this->hasOne(Company::className(), [
				Company::idStr() => 'company',
			]);
		}
	}