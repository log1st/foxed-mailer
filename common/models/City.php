<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 20.08.2017
	 * Time: 18:52
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	use Yii;
	
	/**
	 * Class City
	 * @package common\models
	 *
	 * @property integer $city_id
	 * @property integer $country
	 * @property string $code
	 * @property string $name
	 * @property float $latitude
	 * @property float $longitude
	 *
	 * @property Country $_country
	 */
	class City extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'cities';
		}
		
		public static function tableName()
		{
			return 'cities';
		}
		
		public static function sortAttribute()
		{
			return 'name';
		}
		
		public static function idStr()
		{
			return 'city_id';
		}
		
		public function __toString()
		{
			return (string)$this->name;
		}
		
		public function get_country()
		{
			return $this->hasOne(Country::className(), [
				Country::idStr() => 'country',
			]);
		}
	}