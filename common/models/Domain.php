<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 09.01.2018
	 * Time: 0:19
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	use common\models\auth\Assignment;
	use yii\base\NotSupportedException;
	use yii\web\IdentityInterface;
	
	
	/**
	 * Class Account
	 * @package common\models
	 *
	 * @property integer $account_id
	 * @property string $host
	 * @property integer $company
	 * @property integer $external_id
	 *
	 * @property Company $_company
	 */
	class Domain extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'domains';
		}
		
		public static function tableName()
		{
			return 'domains';
		}
		
		public static function sortAttribute()
		{
			return 'host';
		}
		
		public static function idStr()
		{
			return 'domain_id';
		}
		
		public function __toString()
		{
			return (string)$this->host;
		}
		
		public function get_company()
		{
			return $this->hasOne(Company::className(), [
				Company::idStr() => 'company',
			]);
		}
	}