<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 20.08.2017
	 * Time: 18:52
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	use Yii;
	
	/**
	 * Class Language
	 * @package common\models
	 *
	 * @property integer $language_id
	 * @property string $locale
	 * @property string $name
	 * @property integer $country
	 * @property integer $status
	 *
	 * @property Country $_country
	 */
	class Language extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'languages';
		}
		
		public static function tableName()
		{
			return 'languages';
		}
		
		public static function sortAttribute()
		{
			return 'name';
		}
		
		public static function idStr()
		{
			return 'language_id';
		}
		
		public function __toString()
		{
			return (string)$this->name;
		}
		
		private static $current = null;
		
		public static function current()
		{
			if (is_null(static::$current)) {
				static::$current = Language::findOne([
					'locale' => Yii::$app->language,
				]);
			}
			return static::$current;
		}
		
		public function get_country()
		{
			return $this->hasOne(Country::className(), [
				Country::idStr() => 'country',
			]);
		}
	}