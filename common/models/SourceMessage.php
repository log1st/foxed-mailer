<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 20.08.2017
	 * Time: 18:52
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	
	/**
	 * Class SourceMessage
	 * @package common\models
	 *
	 * @property integer $source_id
	 * @property string $category
	 * @property string $message
	 *
	 * @property Message[] $_translations
	 */
	class SourceMessage extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'source-messages';
		}
		
		public static function tableName()
		{
			return 'source_messages';
		}
		
		public static function idStr()
		{
			return 'source_id';
		}
		
		public function __toString()
		{
			return implode(' » ', [
				$this->category,
				$this->message,
			]);
		}
		
		public function get_translations()
		{
			return $this->hasMany(Message::className(), [
				'source' => self::idStr(),
			]);
		}
	}