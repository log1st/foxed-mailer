<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 20.08.2017
	 * Time: 18:52
	 */
	
	namespace common\models;
	
	
	use common\components\BaseActiveRecord;
	use Yii;
	
	/**
	 * Class Country
	 * @package common\models
	 *
	 * @property integer $country_id
	 * @property integer $idd
	 * @property string $tnp
	 * @property string $currency
	 * @property string $currency_short
	 * @property string $currency_symbol
	 * @property string $code
	 * @property string $iso_code2
	 * @property string $iso_code3
	 * @property string $iso_country_name
	 * @property string $name
	 * @property float $latitude
	 * @property float $longitude
	 * @property integer $status
	 *
	 * @property Language[] $_languages
	 * @property City[] $_cities
	 */
	class Country extends BaseActiveRecord
	{
		public static function cnc()
		{
			return 'countries';
		}
		
		public static function tableName()
		{
			return 'countries';
		}
		
		public static function sortAttribute()
		{
			return 'name';
		}
		
		public static function idStr()
		{
			return 'country_id';
		}
		
		public function __toString()
		{
			return (string)$this->name;
		}
		
		private static $current = null;
		
		public static function current()
		{
			if (is_null(static::$current)) {
			
			}
			return static::$current;
		}
		
		public function get_languages()
		{
			return $this->hasMany(Language::className(), [
				'country' => Country::idStr(),
			]);
		}
		
		public function get_cities()
		{
			return $this->hasMany(City::className(), [
				'country' => Country::idStr(),
			]);
		}
	}