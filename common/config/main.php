<?php
	
	use yii\debug\Module;
	use yii\debug\Panel;
	
	$config = [
		'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
		'sourceLanguage' => '00',
		'language' => 'en-US',
		'components' => \yii\helpers\ArrayHelper::merge([
			'authManager' => [
				'class' => \common\components\i18n\DbManager::className(),
			],
			'view' => [
				'class' => 'common\components\View',
				'renderers' => [
					'twig' => [
						'class' => \common\components\TwigViewRenderer::class,
						'cachePath' => '@runtime/Twig/cache',
						'options' => YII_DEBUG ? [
							'auto_reload' => true,
							'debug' => true,
						] : [],
						'extensions' => YII_DEBUG ? [
							'\Twig_Extension_Debug',
						] : [],
						'globals' => [
							'html' => ['class' => \common\helpers\Html::class],
							'url' => ['class' => \yii\helpers\Url::class],
							'yii' => ['class' => Yii::class],
							'arr' => ['class' => \yii\helpers\ArrayHelper::class],
						],
						'tests' => [
							'string' => function ($item) {
								return is_string($item);
							},
							'boolean' => function ($item) {
								return is_bool($item);
							},
						],
						'filters' => [
							'preg_replace' => function ($item, $pattern, $to = '') {
								return preg_replace($pattern, $to, $item);
							},
						],
						'functions' => [
							't' => 't',
							'pre' => 'pre',
							'mb_ucfirst' => 'mb_ucfirst',
							'shuffle' => function ($item) {
								shuffle($item);
								return $item;
							},
						],
						'uses' => [
							'yii\bootstrap',
						],
					],
				],
			],
			'request' => [
				'enableCookieValidation' => true,
				
				'csrfCookie' => [
					'name' => '_csrf',
					'path' => '/',
					'domain' => "." . CONST_DOMAIN,
				],
			],
			'response' => [
				'format' => yii\web\Response::FORMAT_HTML,
				'on ' . \yii\web\Response::EVENT_BEFORE_SEND => function ($event) {
					/** @var \yii\web\Response $response */
					$response = $event->sender;
					if ($response->format === \yii\web\Response::FORMAT_JSON) {
						$response->statusCode = 200;
						
						$time = microtime(true);
						
						$user = Yii::$app->user;
						/** @var Module|null $debug */
						$log = (YII_DEBUG && !$user->isGuest && $user->can('debug')) ? (Yii::$app->log->getLogger()->messages) : null;
						
						$levels = [
							0x01 => 'error',
							0x02 => 'warning',
							0x04 => 'info',
							0x08 => 'trace',
							0x40 => 'profile',
							0x50 => 'profileBegin',
							0x60 => 'profileEnd'
						];
						
						if ($log) {
							$response->data['debug']['groups'] = [];
							foreach($log as $key => $item) {
								if(isset($log[$key - 1])) {
									$item['time'] = $item[3] - $log[$key - 1][3];
								}
								$response->data['debug']['groups'][$item[2]][$levels[$item[1]]][] = [
									'message' => $item[0],
									'time' => ($item['time'] ?? 0) * 1000
								];
							}
							$response->data['debug']['time'] = (microtime(true) - $time) * 1000;
						}
					}
				},
			],
			'i18n' => [
				'class' => \common\components\i18n\I18N::className(),
				'messageFormatter' => \common\components\i18n\MessageFormatter::className(),
				'translations' => [
					'*' => [
						'class' => \common\components\i18n\DbMessageSource::className(),
						'db' => 'db',
						'messageTable' => 'messages',
						'sourceMessageTable' => 'source_messages',
						
						'enableCaching' => YII_CACHE,
						'cachingDuration' => YII_CACHE_DURATION,
						'forceTranslation' => !YII_CACHE,
						
						'on missingTranslation' => [\common\components\i18n\TranslationEventHandler::class, 'handleMissingTranslation'],
					],
				],
			],
			'assetManager' => [
				'forceCopy' => !YII_CACHE,
				'converter' => [
					'class' => 'yii\web\AssetConverter',
					'forceConvert' => !YII_CACHE,
				],
			],
			'user' => [
				'class' => \common\components\rbac\User::className(),
				'identityClass' => 'common\models\User',
				'enableAutoLogin' => true,
				'loginUrl' => '@toFrontend/sign-in',
				'identityCookie' => [
					'name' => '_identity',
					'httpOnly' => true,
					'domain' => '.' . CONST_DOMAIN,
				],
			],
			'session' => [
				'class' => \common\components\rbac\DbSession::class,
				'sessionTable' => 'sessions',
				
				'cookieParams' => [
					'lifetime' => 10 * 365 * 24 * 60 * 60,
					'domain' => "." . CONST_DOMAIN,
					'httpOnly' => true,
				],
			],
			'errorHandler' => [
				'errorAction' => 'error/index',
			],
			'routesRepository' => [
				'class' => \common\components\RoutesRepository::className(),
			],
			'cache' => [
				'class' => 'yii\caching\FileCache',
				'cachePath' => '@common/runtime/cache',
			],
			'urlManager' => [
				'enablePrettyUrl' => true,
				'showScriptName' => false,
				'enableStrictParsing' => false,
				'rules' => include('rules.php'),
			],
		], require 'db-local.php'),
		'modules' => \yii\helpers\ArrayHelper::merge(YII_DEBUG ? [
			'debug' => [
				'class' => \yii\debug\Module::class,
				'allowedIPs' => ['127.0.0.1', '::1'],
				'traceLine' => '<a href="phpstorm://open?url={file}&line={line}">{file}:{line}</a>',
				'dataPath' => '@runtime/debugbar',
			],
		] : [], [
		
		]),
		'bootstrap' => \yii\helpers\ArrayHelper::merge(YII_DEBUG ? ['debug'] : [], [
			'routesRepository',
		]),
	];
	
	if (YII_DEBUG) {
		$config['components']['log'] = [
			'traceLevel' => 3,
		];
	}
	
	
	return $config;