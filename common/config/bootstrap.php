<?php
	
	Yii::setAlias('@common', dirname(__DIR__));
	Yii::setAlias('@application', dirname(dirname(__DIR__)));
	
	require dirname(__DIR__) . '/../inc/functions.php';
	
	foreach ([
		         ['backend', 'admin'],
		         ['frontend', 'www'],
		         ['static', 'storage'],
		         'common',
		         'console',
	         ] as $item) {
		
		if (!is_string($item)) {
			$path = $item[1] . '.' . (CONST_DOMAIN ?? 'console');
			$host = $item[1];
			$item = $item[0];
		} else {
			$path = $item . '.' . (CONST_DOMAIN ?? 'console');
			$host = $item;
		}
		
		Yii::setAlias('@' . $item, dirname(dirname(__DIR__)) . '/' . ($item !== 'common' ? "modules/" : '') .  $item);
		Yii::setAlias('@' . $item . 'Routes', dirname(dirname(__DIR__)) . '/modules/' . $item . '/config/routes.php');
		Yii::setAlias('@to' . mb_ucfirst($item), "//$path/");
		Yii::setAlias('@' . $item . 'Path', $path);
		Yii::setAlias('@' . $item . 'Host', $host);
	}