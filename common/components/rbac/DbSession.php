<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 24.08.2017
	 * Time: 14:35
	 */
	
	namespace common\components\rbac;
	
	
	use Yii;
	use yii\db\Query;
	use yii\web\ErrorHandler;
	
	class DbSession extends \yii\web\DbSession
	{
		
		public $sessionId = 'hash';
		public $userId = 'user';
		
		
		/**
		 * Composes storage field set for session writing.
		 * @param string $id   session id
		 * @param string $data session data
		 * @return array storage fields
		 */
		protected function composeFields($id, $data)
		{
			$fields = [
				'data' => $data,
			];
			if ($this->writeCallback !== null) {
				$fields = array_merge(
					$fields,
					call_user_func($this->writeCallback, $this)
				);
				if (!is_string($fields['data'])) {
					$_SESSION = $fields['data'];
					$fields['data'] = session_encode();
				}
			}
			// ensure 'id' and 'expire' are never affected by [[writeCallback]]
			$fields = array_merge($fields, [
				$this->sessionId => $id,
				'expire' => time() + $this->getTimeout(),
				$this->userId => Yii::$app->user->id,
				'created_at' => time(),
				'updated_at' => time(),
			]);
			return $fields;
		}
		
		/**
		 * @inheritdoc
		 */
		public function regenerateID($deleteOldSession = false)
		{
			$oldID = session_id();
			
			if (empty($oldID)) {
				return;
			}
			
			
			if ($this->getIsActive()) {
				if (YII_DEBUG && !headers_sent()) {
					session_regenerate_id($deleteOldSession);
				} else {
					@session_regenerate_id($deleteOldSession);
				}
			}
			
			$newID = session_id();
			// if session id regeneration failed, no need to create/update it.
			if (empty($newID)) {
				Yii::warning('Failed to generate new session ID', __METHOD__);
				return;
			}
			
			$query = new Query();
			$row = $query->from($this->sessionTable)
				->where([$this->sessionId => $oldID])
				->createCommand($this->db)
				->queryOne();
			if ($row !== false) {
				if ($deleteOldSession) {
					$this->db->createCommand()
						->update($this->sessionTable, [$this->sessionId => $newID, 'updated_at' => time()], [$this->sessionId => $oldID])
						->execute();
				} else {
					$row[$this->sessionId] = $newID;
					$row[$this->userId] = Yii::$app->user->id;
					$this->db->createCommand()
						->insert($this->sessionTable, $row)
						->execute();
				}
			} else {
				// shouldn't reach here normally
				$this->db->createCommand()
					->insert($this->sessionTable, $this->composeFields($newID, ''))
					->execute();
			}
		}
		
		/**
		 * @inheritdoc
		 */
		public function readSession($id)
		{
			$query = new Query();
			$query->from($this->sessionTable)
				->where('[[expire]]>:expire AND [[' . $this->sessionId . ']]=:id', [':expire' => time(), ':id' => $id]);
			
			if ($this->readCallback !== null) {
				$fields = $query->one($this->db);
				return $fields === false ? '' : $this->extractData($fields);
			}
			
			$data = $query->select(['data'])->scalar($this->db);
			return $data === false ? '' : $data;
		}
		
		/**
		 * @inheritdoc
		 */
		public function writeSession($id, $data)
		{
			try {
				$query = new Query;
				$exists = $query->select([$this->sessionId])
					->from($this->sessionTable)
					->where([$this->sessionId => $id])
					->createCommand($this->db)
					->queryScalar();
				$fields = $this->composeFields($id, $data);
				if ($exists === false) {
					$this->db->createCommand()
						->insert($this->sessionTable, $fields)
						->execute();
				} else {
					unset($fields[$this->sessionId]);
					$this->db->createCommand()
						->update($this->sessionTable, $fields, [$this->sessionId => $id])
						->execute();
				}
			} catch (\Exception $e) {
				$exception = ErrorHandler::convertExceptionToString($e);
				error_log($exception);
				if (YII_DEBUG) {
					echo $exception;
				}
				return false;
			}
			
			return true;
		}
		
		/**
		 * @inheritdoc
		 */
		public function destroySession($id)
		{
			$this->db->createCommand()
				->delete($this->sessionTable, [$this->sessionId => $id])
				->execute();
			
			return true;
		}
	}