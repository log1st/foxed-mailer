<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 11.01.2018
	 * Time: 0:48
	 */
	
	namespace common\components;
	
	
	use Yii;
	
	class Application extends \yii\web\Application
	{
		private $_vendorPath;
		
		public function setVendorPath($path)
		{
			$this->_vendorPath = Yii::getAlias($path);
			Yii::setAlias('@vendor', $this->_vendorPath);
			Yii::setAlias('@bower', $this->_vendorPath . DIRECTORY_SEPARATOR . 'bower-asset');
			Yii::setAlias('@npm', $this->_vendorPath . DIRECTORY_SEPARATOR . 'npm');
		}
	}