<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 29.08.2017
	 * Time: 0:57
	 */
	
	namespace common\components;
	
	
	use Yii;
	use yii\base\Component;
	use yii\web\UrlManager;
	
	/**
	 * Class RoutesRepository
	 * @package common\components
	 *
	 * @property UrlManager[] $routes
	 */
	class RoutesRepository extends Component
	{
		public $routes = [];
		
		public function getRoutes()
		{
			$routes = [];
			foreach (Yii::$aliases as $key => $alias) {
				if ($match = preg_match('/^@([\w\d]+)(Routes)$/', $key, $matches)) {
					if (is_file($alias)) {
						$routes[$matches[1]] = include(Yii::getAlias($alias));
					}
				}
			}
			
			$def = Yii::$app->urlManager;
			
			foreach ($routes as $key => $route) {
				if (is_array($route)) {
					$this->routes[$key] = new UrlManager([
						'enablePrettyUrl' => $def->enablePrettyUrl,
						'showScriptName' => $def->showScriptName,
						'enableStrictParsing' => $def->enableStrictParsing,
						'suffix' => $def->suffix,
						'rules' => $route,
						'customHost' => Yii::getAlias("@{$key}Host"),
					]);
				}
			}
		}
		
		public function init()
		{
			$this->getRoutes();
		}
	}