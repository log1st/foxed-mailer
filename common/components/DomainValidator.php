<?php
	/**
	 * Created by PhpStorm.
	 * User: log1st
	 * Date: 06.02.2018
	 * Time: 23:21
	 */
	
	namespace common\components;
	
	
	use yii\validators\Validator;
	
	class DomainValidator extends Validator
	{
		public $allowPath = true;
		
		public function validateAttribute($model, $attribute)
		{
			$url = $model->{$attribute};
			
			$parse = parse_url($url);
			
			if($parse && isset($parse['host'])) {
				$url = $parse['host'];
			}
			
			if($this->allowPath && isset($parse['path'])) {
				$url .= $parse['path'];
			}
			
			$model->{$attribute} = $url;
		}
	}