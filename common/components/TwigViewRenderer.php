<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 02.11.2017
	 * Time: 19:44
	 */
	
	namespace common\components;
	
	
	use yii\twig\ViewRenderer;
	
	class TwigViewRenderer extends ViewRenderer
	{
		public $tests = [];
		
		public function addTests($tests)
		{
			foreach ($tests as $key => $test) {
				$this->twig->addTest(new \Twig_SimpleTest($key, $test));
			}
		}
		
		public function init()
		{
			parent::init();
			
			if (!empty($this->tests)) {
				$this->addTests($this->tests);
			}
		}
	}