<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 22.10.2017
 * Time: 11:41
 */

namespace common\components\db;


class Query extends \yii\db\Query
{
    public function lists($id, $value = null) {
        $array = [];

        foreach($this->all() as $key => $item) {
            $array[$value ? ($item[$id] ?? $key) : $key] = $value ? ($item[$value] ?? $item[$id] ?? null) : ($item[$id] ?? null);
        }

        return $array;
    }
}