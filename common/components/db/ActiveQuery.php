<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 22.10.2017
	 * Time: 11:41
	 */
	
	namespace common\components\db;
	
	
	class ActiveQuery extends \yii\db\ActiveQuery
	{
		public function lists($id, $value = null)
		{
			$array = [];
			
			foreach ($this->all() as $key => $item) {
				$array[ $value ? ($item[ $id ] ?? $key) : $key ] = $value ? ($value === true ? $item->__toString() : ($item[ $value ] ?? $item[ $id ] ?? null)) : ($item[ $id ] ?? null);
			}
			
			return $array;
		}
	}