<?php
	
	namespace common\components\i18n;
	
	use yii;
	use yii\db\Query;
	use yii\i18n\MissingTranslationEvent;
	
	/**
	 * Events on I18n translation
	 * Class TranslationEventHandler
	 * @package app\components
	 */
	class TranslationEventHandler
	{
		public static function handleMissingTranslation(MissingTranslationEvent $event)
		{
			$category = $event->category;
			$message = $event->message;
			$db = Yii::$app->db;
			
			/** @var DbMessageSource $sender */
			$sender = $event->sender;
			
			if (!YII_CACHE) {
				$query = (new Query)
					->select([
						'*',
					])
					->from([
						'source_messages',
					])
					->where([
						'category' => $category,
						'message' => $message,
					])
					->one();
				
				if (!isset($query['source_id'])) {
					$db->createCommand()->insert('source_messages', [
						'category' => $category,
						'message' => $message,
						'created_at' => time(),
						'updated_at' => time(),
					])->execute();
				} else {
				
				}
			}
		}
	}