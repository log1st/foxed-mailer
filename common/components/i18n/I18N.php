<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.02.2017
	 * Time: 9:55
	 */
	
	namespace common\components\i18n;
	
	
	use Yii;
	
	class I18N extends \yii\i18n\I18N
	{
		/**
		 * Extend Yii::t function to set categories more standardized
		 * @param string $category
		 * @param string $message
		 * @param array $params
		 * @param string $language
		 * @return string
		 */
		public function translate($category, $message, $params, $language)
		{
			if (is_null($category)) {
				$backtrace = debug_backtrace();
				foreach ($backtrace as $step) {
					if ($step['function'] == 't') {
						$file = $step['file'];
						$root = Yii::getAlias('@common');
						$root = explode('/', $root);
						unset($root[count($root) - 1]);
						$root = implode('/', $root);
						
						$file = str_replace($root, '', $file);
						$file = explode('/', $file);
						
						if (!in_array($file[1], [
							'vendor',
						])
						) {
							$category = implode('/', $file);
						}
					}
				}
			}
			if (!in_array($category, ['yii', 'http']) && !is_array($category)) {
				$category = Yii::$app->id . '::' . $category;
			}
			if (is_array($category)) {
				$category = $category[0];
			}
			return parent::translate($category, $message, $params, $language);
		}
	}