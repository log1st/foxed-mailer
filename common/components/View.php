<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 14.08.2017
	 * Time: 5:02
	 */
	
	namespace common\components;
	
	
	use yii\helpers\ArrayHelper;
	use yii\helpers\Url;
	
	class View extends \yii\web\View
	{
		/**
		 * List of breadcrumbs
		 * @var array
		 */
		public $breadcrumbs = [];
		
		/**
		 * Substrings to title
		 * @var array
		 */
		public $substrings = [];
		
		/**
		 * SubTitle
		 * @var string
		 */
		public $subTitle = '';
		
		/**
		 * SubTitle options
		 * @var array
		 */
		public $subTitleOptions = [];
		
		/**
		 * @inheritdoc
		 */
		public $defaultExtension = 'twig';
		
		/** @var mixed */
		public $data = null;
		
		public function render($view, $params = [], $context = null)
		{
			return parent::render($view, $params, $context);
		}
	}