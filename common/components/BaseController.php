<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 08.01.2018
	 * Time: 12:01
	 */
	
	namespace common\components;
	
	
	use common\helpers\ArrayHelper;
	use Yii;
	use yii\debug\Module;
	use yii\debug\Panel;
	use yii\web\Controller;
	
	class BaseController extends Controller
	{
		
		public function response($status, $data = null)
		{
			return $this->asJson(ArrayHelper::merge([
				'status' => $status,
			], $data ? [
				'data' => $data,
			] : []));
		}
		
	}