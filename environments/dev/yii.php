#!/usr/bin/env php
<?php
require __DIR__ . '/common/config/bootstrap-local.php';

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/common/config/bootstrap.php';
require __DIR__ . '/modules/console/config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/common/config/main.php',
    require __DIR__ . '/common/config/main-local.php',
    require __DIR__ . '/modules/console/config/main.php',
    require __DIR__ . '/modules/console/config/main-local.php'
);

foreach ([
             'request',
             'user',
             'session',
             'errorHandler',
             'response'
         ] as $item) {
    if (isset($config['components'][$item])) {
        unset($config['components'][$item]);
    }
    if($key = array_search($item, $config['bootstrap'])) {
        unset($config['bootstrap'][$key]);
    }
}

$application = new yii\console\Application($config);
$exitCode = $application->run();
exit($exitCode);