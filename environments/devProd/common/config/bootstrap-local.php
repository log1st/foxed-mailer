<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 14.08.2017
	 * Time: 4:00
	 */
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_ENV') or define('YII_ENV', 'dev');
	
	defined('YII_CACHE') or define('YII_CACHE', false);
	defined('YII_CACHE_DURATION') or define('YII_CACHE_DURATION', 60 * 60);
	
	defined('CONST_DOMAIN') || define('CONST_DOMAIN', 'smtp.log1st.net');