<?php
	$base = [
		'setWritable' => [
			'common/runtime',
			
			'modules/console/runtime',
			
			'modules/backend/runtime',
			'modules/backend/web/assets',
			'modules/backend/web/dist',
			'modules/backend/web',
			
			'modules/frontend/runtime',
			'modules/frontend/web/assets',
			'modules/frontend/web/dist',
			'modules/frontend/web',
		],
		'setExecutable' => [
			'yii.php',
		],
		'setCookieValidationKey' => [
			'common/config/main-local.php',
		],
	];
	
	return [
		'Development' => ['path' => 'dev'] + $base,
		'Development on prod' => ['path' => 'devProd'] + $base,
	];